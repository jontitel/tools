﻿Imports System.Xml

Module Main
    Private Structure CommandArguments
        Dim Path As String
        Dim FileMask As String
        Dim FixAuditType As String
        Dim DeleteSource As Boolean
        Dim ZipResult As Boolean
    End Structure
    Sub Main()
        Dim runArgs As CommandArguments
        Dim idx As Integer
        Dim baseXML As XmlDocument
        Dim nextXML As XmlDocument
        Dim combinedFileName As String

        Try
            baseXML = New XmlDocument()
            nextXML = New XmlDocument

            ' Load the command line parameters
            runArgs = LoadCommandArguments(Command)

            ' Open the first file
            If IO.Directory.GetFiles(runArgs.Path, runArgs.FileMask).Count > 0 Then
                baseXML.Load(IO.Directory.GetFiles(runArgs.Path, runArgs.FileMask)(0))

                ' List the file that match the mask supplied in the command line
                For idx = 1 To IO.Directory.GetFiles(runArgs.Path, runArgs.FileMask).Count - 1
                    ' Load the xml from the file
                    nextXML.Load(IO.Directory.GetFiles(runArgs.Path, runArgs.FileMask)(idx))
                    ' Append nodes from subsequent files to the first file
                    baseXML = AppendChildNodes(baseXML, nextXML)
                Next

                If Not runArgs.FixAuditType = "" Then
                    baseXML = FixAudit(baseXML, runArgs.FixAuditType)
                End If

                ' Save the combined file
                combinedFileName = runArgs.FileMask.Replace("*", "_Combined_" & Date.UtcNow.ToString("yyyyMMddhhmmss"))
                baseXML.Save(IO.Path.Combine(runArgs.Path, combinedFileName))

                ' Zip the combine file (if configured
                ZipFiles(runArgs.Path & IO.Path.GetFileNameWithoutExtension(combinedFileName) & ".zip", runArgs.Path & combinedFileName)

                ' Delete the source files if DeleteSource flag set
                If runArgs.DeleteSource Then
                    For Each file In IO.Directory.GetFiles(runArgs.Path, runArgs.FileMask)
                        If IO.Path.GetFileName(file) = combinedFileName Then
                            If runArgs.ZipResult = False Then
                                ' Save the file for transmission
                            Else
                                IO.File.Delete(file)
                            End If
                        Else
                            IO.File.Delete(file)
                        End If
                    Next
                End If
            Else
                Console.Write("No files found in " & runArgs.Path & " with a name matching " & runArgs.FileMask)
            End If

        Catch handledException As Exception
            WriteLog("Error in Main. " & handledException.ToString)
        End Try
    End Sub
    Private Function AppendChildNodes(BaseXML As XmlDocument, SourceXML As XmlDocument) As XmlDocument
        Dim childNode As XmlNode
        Try
            For Each childNode In SourceXML.DocumentElement.ChildNodes()
                BaseXML.DocumentElement.AppendChild(BaseXML.ImportNode(childNode, True))
            Next

            Return BaseXML
        Catch handledException As Exception
            WriteLog("Error in AppendChildNodes. " & handledException.ToString)
        End Try
    End Function
    Private Function FixAudit(BaseXML As XmlDocument, AuditType As String) As XmlDocument
        Dim audit As XmlElement
        Dim badNode As XmlNode
        Try
            Select Case AuditType.ToLower
                Case "code"
                    audit = FixAudit_Code(BaseXML)
                Case "vendor"
                    audit = FixAudit_Vendor(BaseXML)
                Case "status"
                    audit = FixAudit_Status(BaseXML)
            End Select

            ' Pull the incorrect Audit nodes
            For Each badNode In BaseXML.SelectNodes("//Audit")
                BaseXML.DocumentElement.RemoveChild(badNode)
            Next

            ' Add the new updated Audit node
            BaseXML.DocumentElement.AppendChild(audit)

            Return BaseXML

        Catch handledException As Exception
            WriteLog("Error in FixAudit. " & handledException.ToString)
        End Try

    End Function
    Private Function FixAudit_Code(BaseXML As XmlDocument) As XmlNode
        Dim audit As XmlElement
        Dim codeCount As XmlElement

        Try
            ' Create Audit Node header
            audit = BaseXML.CreateElement("Audit")

            ' Create sub node for count of code nodes and populate with count
            codeCount = BaseXML.CreateElement("TotalCodesCount")
            codeCount.InnerText = BaseXML.SelectNodes("//Code").Count
            audit.AppendChild(codeCount)

            Return audit

        Catch handledException As Exception
            WriteLog("Error in FixAudit_Code. " & handledException.ToString)
        End Try
    End Function
    Private Function FixAudit_Vendor(BaseXML As XmlDocument) As XmlNode
        Dim audit As XmlElement
        Dim vendorCount As XmlElement
        Dim buList As SortedList
        Dim buCount As XmlElement
        Dim businessUnit As XmlElement

        Try

            ' Create Audit Node header
            audit = BaseXML.CreateElement("Audit")

            ' Create sub node for count of code nodes and populate with count
            vendorCount = BaseXML.CreateElement("VendorCount")
            vendorCount.InnerText = BaseXML.SelectNodes("//Vendor").Count
            audit.AppendChild(vendorCount)

            ' Count the unique Business Units
            buList = New SortedList
            For Each businessUnit In BaseXML.SelectNodes("//BusinessUnitID")
                If Not buList.ContainsKey(businessUnit.InnerText) Then
                    buList.Add(businessUnit.InnerText, businessUnit.InnerText)
                End If
            Next

            ' Create sub node for count of code nodes and populate with count
            buCount = BaseXML.CreateElement("BusinessUnitCount")
            buCount.InnerText = buList.Count.ToString
            audit.AppendChild(buCount)

            Return audit

        Catch handledException As Exception
            WriteLog("Error in FixAudit_Code. " & handledException.ToString)
        End Try
    End Function
    Private Function FixAudit_Status(BaseXML As XmlDocument) As XmlNode
        Dim audit As XmlElement
        Dim statusCount As XmlElement
        Dim auditTotal As XmlElement
        Dim total As Decimal
        Dim totalDue As XmlElement

        Try

            ' Create Audit Node header
            audit = BaseXML.CreateElement("Audit")

            ' Create sub node for count of code nodes and populate with count
            statusCount = BaseXML.CreateElement("InvoiceCount")
            statusCount.InnerText = BaseXML.SelectNodes("//Invoice").Count
            audit.AppendChild(statusCount)

            ' Count the unique Business Units
            For Each totalDue In BaseXML.SelectNodes("//InvoiceAmount")
                total += Convert.ToDecimal(totalDue.InnerText)
            Next

            ' Create sub node for count of code nodes and populate with count
            auditTotal = BaseXML.CreateElement("InvoiceAmountTotal")
            auditTotal.InnerText = total.ToString("####.00")
            audit.AppendChild(auditTotal)

            Return audit

        Catch handledException As Exception
            WriteLog("Error in FixAudit_Code. " & handledException.ToString)
        End Try
    End Function
    Private Function LoadCommandArguments(CommandArguments As String) As CommandArguments
        Dim argParts() As String
        Dim runArgs As CommandArguments
        Dim idx As Integer
        Dim invalidCmdArgs As Boolean

        Try
            runArgs = New CommandArguments
            ' Set Defaults
            runArgs.DeleteSource = False
            invalidCmdArgs = False

            If Command.Length = 0 Then
                invalidCmdArgs = True
            Else
                argParts = Command.Split(" ")
                ' Check that all arguments are provide
                Do While idx < argParts.Length
                    Select Case argParts(idx)
                        Case "/path"
                            runArgs.Path = argParts(idx + 1)
                            If Not runArgs.Path.EndsWith("\") Then
                                runArgs.Path = runArgs.Path & "\"
                            End If
                            idx += 2
                        Case "/mask"
                            runArgs.FileMask = argParts(idx + 1)
                            idx += 2
                        Case "/fixaudit"
                            runArgs.FixAuditType = argParts(idx + 1)
                            idx += 2
                        Case "/deletesource"
                            If argParts(idx + 1).ToLower = "TRUE".ToLower Then
                                runArgs.DeleteSource = True
                            Else
                                runArgs.DeleteSource = False
                            End If
                            idx += 2
                        Case "/zipfile"
                            If argParts(idx + 1).ToLower = "TRUE".ToLower Then
                                runArgs.ZipResult = True
                            Else
                                runArgs.ZipResult = False
                            End If
                            idx += 2
                        Case Else
                            invalidCmdArgs = True
                            idx += 2
                    End Select
                Loop
            End If

            If invalidCmdArgs = True Then
                Console.WriteLine("Invalid command arguments.  Valid Options are:")
                Console.WriteLine("/path Input Path for Files to Combine")
                Console.WriteLine("/mask Filemask for files to combine.  Standard file wildcards supported.")
                Console.WriteLine("/depth Node depth for child inclusion.  Must be 1 or greater.  1 = First level below document element.")
                Console.WriteLine("/deletesource True|False  Will delete source files once combined file is saved.")
                Console.WriteLine("/path, /mask are required. Default is /depth = 1 and /deletesource = False")
                Throw New Exception("Invalid Command Line Parameters used")
            End If

            Return runArgs

        Catch handledException As Exception
            WriteLog("Error in LoadCommandArguments. " & handledException.ToString)
        End Try

    End Function
    Private Sub WriteLog(Message As String)
        Try
            Console.WriteLine(Message)
        Catch handledException As Exception
            Console.WriteLine("Error in WriteLog. " & handledException.ToString)
        End Try

    End Sub
    Private Function ZipFiles(ByVal ZipFileName As String, ByVal ZipFileSet As String) As Integer
        ' uses the Xceed.Zip, and iPayables Encryption components
        Dim LicKey As String
        Dim Decrypt As Encryption.Decryptor

        Try
            ' get the encrypted key
            LicKey = Configuration.ConfigurationManager.AppSettings.Get("ZipLicenseKey")
            ' decrypt it
            Decrypt = New Encryption.Decryptor
            LicKey = System.Text.Encoding.ASCII.GetString(Decrypt.Decrypt(LicKey))
            ' set the Xceed.Zip key
            Xceed.Zip.Licenser.LicenseKey = LicKey
            ' zip the file set
            Xceed.Zip.QuickZip.Zip(ZipFileName, True, True, False, ZipFileSet)
            Return 1

        Catch handledException As System.Exception
            WriteLog("Error in ZipFiles. " & handledException.ToString)
        Finally
            Decrypt = Nothing
        End Try
    End Function

End Module
