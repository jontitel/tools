--DROP TABLE dbo.IPCompanySettings
CREATE TABLE dbo.IPCompanySettings(
	ID INT IDENTITY
  ,	DynamicsCompanyID INT
  , CustomerCompanyID NVARCHAR(20)
  ,	DatabaseName NVARCHAR(128)
  ,	CheckBook NVARCHAR(50)
  , OverridePostingDate INT  
  , DefaultAPAccount NVARCHAR(20)
  , UsesPurchaseOrder INT
    )