DROP PROCEDURE [usp_iPayables_GetGLAccounts]
GO	
CREATE PROCEDURE [usp_iPayables_GetGLAccounts]
@databaseName nvarchar(50)
AS
BEGIN
DECLARE @sql NVARCHAR(MAX)

SET @sql = 'SELECT RTRIM(ACTNUMST) ACTNUMST, RTRIM(ACTDESCR) ACTDESCR
FROM ' + @databaseName + '.dbo.GL00105 JOIN
	' + @databaseName + '.dbo.GL00100 ON GL00105.ACTINDX = GL00100.ACTINDX'

	EXEC (@sql)	
END