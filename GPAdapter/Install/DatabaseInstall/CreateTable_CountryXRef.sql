CREATE  TABLE CountryXRef(
	ID INT IDENTITY
,	GPCountry VARCHAR(50)
,	ISOTwo char(2)
,	Description VARCHAR(100)
)
