CREATE PROCEDURE dbo.usp_iPayables_CompanySettings_Insert
    @GPCompanyID INT ,
    @CustomerCompanyID NVARCHAR(20) ,
    @DatabaseName NVARCHAR(20) ,
    @DefaultCheckbook NVARCHAR(20) ,
    @OverridePostingDate INT ,
    @DefaultAPAccount NVARCHAR(20)
AS 
    INSERT  INTO dbo.IPCompanySettings
            ( DynamicsCompanyID ,
              CustomerCompanyID ,
              DatabaseName ,
              CheckBook ,
              OverridePostingDate,
			  DefaultAPAccount
            )
    VALUES  ( @GPCompanyID ,
              @CustomerCompanyID ,
              @DatabaseName ,
              @DefaultCheckbook ,
              @OverridePostingDate ,
              @DefaultAPAccount
            )
        