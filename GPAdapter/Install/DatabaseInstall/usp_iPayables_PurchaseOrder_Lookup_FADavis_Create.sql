--EXEC usp_iPayables_PurchaseOrder_Lookup 'FAD', 3
DROP PROC usp_iPayables_PurchaseOrder_Lookup
go
CREATE PROC dbo.usp_iPayables_PurchaseOrder_Lookup
    @databaseName VARCHAR(50),
	@daysBack INT = 3
AS 
        DECLARE @sql VARCHAR(MAX)

SET @sql = '
DECLARE @classLookup TABLE(
Code VARCHAR(5),
Description VARCHAR(50)
)

INSERT INTO @classLookup VALUES  ( ''1'',''Marketing'')
INSERT INTO @classLookup VALUES  ( ''2'',''Production Printing'')
INSERT INTO @classLookup VALUES  ( ''3'',''Paper'')
INSERT INTO @classLookup VALUES  ( ''4'',''Production Other (not HP/Nursing)'')
INSERT INTO @classLookup VALUES  ( ''5'',''Production Other (HP/Nursing)'')
INSERT INTO @classLookup VALUES  ( ''6'',''MIS (Management Infomation Systems)'')

DECLARE @poLine TABLE(			
			PONbr  CHAR(17), 
			POLineNbr INT,
			Quantity DECIMAL(18,4),
			UnitOfMeasure CHAR(9),
			ItemID_SKU CHAR(31),
			Description CHAR(101),
			UnitCost DECIMAL(18,4),
			LineTotal DECIMAL(18,2),
			LineStatus INT
);

WITH POPPOCTE (PONUMBER, QTYORDER, UOFM, ITEMNMBR, ITEMDESC, UNITCOST, EXTDCOST, POLNESTA, ORD) AS (
SELECT pol.PONUMBER, QTYORDER, UOFM, ITEMNMBR, ITEMDESC, UNITCOST, EXTDCOST, POLNESTA, ORD FROM FADGP.dbo.POP10110 pol
JOIN FADGP.dbo.POP10100 poh ON poh.PONUMBER = pol.PONUMBER
WHERE  poh.MODIFDT > ''1/1/2012''
--WHERE  poh.MODIFDT > DATEADD(DAY, ' + CAST(@daysBack * -1 AS VARCHAR(10)) + ', GETUTCDATE())
)
INSERT INTO @poLine
        ( PONbr ,
          POLineNbr ,
          Quantity ,
          UnitOfMeasure ,
          ItemID_SKU ,
          Description ,
          UnitCost ,
          LineTotal,
		  LineStatus
        )
SELECT PONUMBER
, ROW_NUMBER() OVER(PARTITION BY PONUMBER ORDER BY PONUMBER, ORD ASC) AS ROWNUMBER
  , QTYORDER, UOFM, ITEMNMBR, ITEMDESC, UNITCOST, EXTDCOST, POLNESTA
FROM POPPOCTE;			

declare @FADPOTotal TABLE(
			PONbr  CHAR(17), 
			POTotal DECIMAL(18,2) 
)

insert into @FADPOTotal
select ponumber, sum(line_total)
FROM FADGP.dbo.FAD_IPAYABLES
GROUP BY ponumber

SELECT poh.VENDORID
,	poh.PONUMBER
,	poh.DOCDATE
,	ISNULL(fadpot.POTotal, poh.SUBTOTAL + poh.TRDISAMT + poh.FRTAMNT + poh.MSCCHAMT + poh.TAXAMNT) AS TOTALDUE
,	ISNULL(iptx.CustomerTermCode,''NET030'') AS PYMTRMID
,	ISNULL(curxr.ISOThree,''USD'') AS CURNCYID
,	'''' as SHIPTO_NAME
,	ISNULL(fadip.delivery_address1,'''') AS SHIPTO_ADDRESS1
,	ISNULL(fadip.delivery_address2,'''') AS SHIPTO_ADDRESS2
,	ISNULL(fadip.delivery_address_city,'''') AS SHIPTO_CITY
,	ISNULL(fadip.delivery_address_state,'''') AS SHIPTO_STATE
,	ISNULL(fadip.delivery_address_zip,'''') as SHIPTO_ZIPCODE
,	ISNULL(fadip.delivery_address_country,''US'') AS SHIPTO_COUNTRY
,	'''' AS SHIPTO_PHONE1
,	'''' AS SHIPTO_PHONE2
,	billto.LOCATNNM as BILLTO_NAME
,	billto.ADDRESS1 AS BILLTO_ADDRESS1
,	billto.CITY AS BILLTO_CITY
,	billto.STATE AS BILLTO_STATE
,	billto.ZIPCODE as BILLTO_ZIPCODE
,	ISNULL(cxr.isotwo,''US'') AS BILLTO_COUNTRY  -- comes from exteral table
,	billto.PHONE1 AS BILLTO_PHONE1
,	billto.PHONE2 AS BILLTO_PHONE2
,   CASE POSTATUS
		WHEN 1 THEN ''OPEN''
		WHEN 2 THEN ''OPEN''
		WHEN 3 THEN ''OPEN''
		WHEN 4 THEN ''CLSD''
		WHEN 5 THEN ''CLSD''
		WHEN 6 THEN ''CLSD''
		ELSE ''OPEN''
	END AS STATUS
,	pol.POLineNbr
,	ISNULL(fadip.qty_ordered,pol.Quantity) AS Quantity
,	pol.UnitOfMeasure
,	pol.ItemID_SKU
,	pol.Description
,	ISNULL(fadip.unit_price,pol.UnitCost) AS UnitCost
,	ISNULL(FADIP.line_total, pol.LineTotal) as LineTotal
,	ISNULL(fadip.po_class, poh.POTYPE) AS POTYPE
,   ISNULL(cl.Description,'''') AS HCF3
,	ISNULL(fadip.requestor_email,'''') as HCF1
,	ISNULL(fadip.Cost_code,'''') LCF1
,	ISNULL(fadip.reprint_number,'''') AS LCF2
,	ISNULL(fadip.Item_reference,'''') as LCF3
,	ISNULL(fadip.Fixed_variable,'''') as LCF4
,   ISNULL(fadip.qty_received,0) as QuantityReceived
,   CASE LineStatus
		WHEN 1 THEN ''OPEN''
		WHEN 2 THEN ''OPEN''
		WHEN 3 THEN ''OPEN''
		WHEN 4 THEN ''CLSD''
		WHEN 5 THEN ''CLSD''
		WHEN 6 THEN ''CLSD''
		ELSE ''OPEN''
	END AS LINESTATUS
FROM  FADGP.dbo.POP10100 poh
--LEFT OUTER JOIN FADGP.dbo.SY00600 shipto ON poh.PRSTADCD = shipto.LOCATNID
LEFT OUTER JOIN FADGP.dbo.SY00600 billto ON poh.PRBTADCD = billto.LOCATNID
JOIN @poLine pol ON poh.PONUMBER = pol.PONbr 
LEFT OUTER JOIN CountryXRef cxr ON billto.COUNTRY = cxr.GPCountry
LEFT OUTER JOIN FADGP.dbo.FAD_IPAYABLES fadip on poh.PONUMBER = fadip.ponumber and pol.PONbr = fadip.ponumber and pol.POLineNbr = fadip.linenumber
LEFT OUTER JOIN @FADPOTotal fadpot on fadpot.PONbr = poh.PONUMBER
LEFT OUTER JOIN @classLookup cl ON fadip.po_class = Code
LEFT OUTER JOIN iPayablesTermXRef iptx on poh.PYMTRMID = iptx.GPTermID
LEFT OUTER JOIN CurrencyXRef curxr on poh.CURNCYID = curxr.GPCurrencyID
--WHERE poh.MODIFDT > DATEADD(DAY,' + CAST(@daysBack * -1 AS VARCHAR(10)) + ', GETUTCDATE())'

        exec (@sql)

GO
