CREATE PROCEDURE dbo.usp_iPayables_CompanySettings_Lookup
    @CustomerCompanyID NVARCHAR(20) = ''
AS 
    IF ( @CustomerCompanyID = '' ) 
        BEGIN
            SELECT TOP 1
                    *
            FROM    dbo.IPCompanySettings ics
        END 
    ELSE 
        BEGIN
            SELECT  *
            FROM    dbo.IPCompanySettings ics
            WHERE   CustomerCompanyID = @CustomerCompanyID
        END	   