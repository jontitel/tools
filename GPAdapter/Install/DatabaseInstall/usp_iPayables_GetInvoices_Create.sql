DROP PROCEDURE [usp_iPayables_GetInvoices]
GO	
CREATE PROCEDURE [usp_iPayables_GetInvoices]
@databaseName nvarchar(50)
AS
BEGIN
DECLARE @sql NVARCHAR(MAX)

SET @sql = 'SELECT  RTRIM(i.VENDORID) CustomersVendorNbr, ''CASHC'' BusinessUnitID, RTRIM(i.DOCNUMBR) InvoiceNbr, i.DOCDATE InvoiceDate, 
	RTRIM(i.VCHRNMBR) VoucherNbr, RTRIM(i.PORDNMBR) PONbr, i.DUEDATE InvoiceDueDate, i.POSTEDDT PaymentDate, 
	CASE
		WHEN v.PYMNTPRI LIKE ''IPY'' THEN ''BCHK''
		ELSE ''CHCK''
	END PaymentType, 
	 CAST(i.DOCAMNT AS DECIMAL(18,2)) InvoiceAmount, CAST(i.DISCAMNT AS DECIMAL(18,2)) DiscountAmount, 
	 CAST(i.DOCAMNT AS DECIMAL(18,2))  NetAmount, CAST(i.DOCAMNT AS DECIMAL(18,2))  PaymentAmount, ISNULL(RTRIM(i.CURNCYID), ''USD'') Currency,
	CASE
		WHEN v.PYMNTPRI LIKE ''IPY'' THEN ''PEND''
		ELSE ''PAID''
	END [Status],
	RTRIM(p.APFRDCNM) PaymentReferenceNbr
		FROM ' + @databaseName + '.dbo.PM30200 i LEFT OUTER JOIN
			 ' + @databaseName + '.dbo.PM30300 p ON i.VCHRNMBR = p.APTVCHNM JOIN
			 ' + @databaseName + '.dbo.PM00200 v ON i.VENDORID = v.VENDORID
	WHERE i.DOCTYPE LIKE 1		
		AND p.DOCDATE > DATEADD(DAY,-90,GETDATE())
		AND i.BACHNUMB NOT LIKE ''IPY_PAID''
	ORDER BY i.DOCDATE DESC'

	EXEC (@sql)
END