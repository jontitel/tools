--EXEC usp_iPayables_PurchaseOrder_Lookup 'TWO', 3
DROP PROC usp_iPayables_PurchaseOrder_Lookup
go
CREATE PROC dbo.usp_iPayables_PurchaseOrder_Lookup

 @databaseName VARCHAR(50),
 @daysBack INT = 3
AS 
       DECLARE @sql VARCHAR(MAX)

SET @sql = '
DECLARE @poLine TABLE
    (
      PONbr CHAR(17) ,
      POLineNbr INT ,
      Quantity DECIMAL(18, 4) ,
      UnitOfMeasure CHAR(9) ,
      ItemID_SKU CHAR(31) ,
      Description CHAR(101) ,
      UnitCost DECIMAL(18, 4) ,
      LineTotal DECIMAL(18, 2) ,
      LineStatus INT
    );
	
WITH    POPPOCTE ( PONUMBER, QTYORDER, UOFM, ITEMNMBR, ITEMDESC, UNITCOST, EXTDCOST, POLNESTA, ORD )
          AS ( SELECT   pol.PONUMBER ,
                        QTYORDER ,
                        UOFM ,
                        ITEMNMBR ,
                        ITEMDESC ,
                        UNITCOST ,
                        EXTDCOST ,
                        POLNESTA ,
                        ORD
               FROM     ' + @databaseName + '.dbo.POP10110 pol
                        JOIN ' + @databaseName + '.dbo.POP10100 poh ON poh.PONUMBER = pol.PONUMBER
               WHERE    poh.MODIFDT > ''1/1/2012''
--WHERE  poh.MODIFDT > DATEADD(DAY, -3, GETUTCDATE())
             )
    INSERT  INTO @poLine
            ( PONbr ,
              POLineNbr ,
              Quantity ,
              UnitOfMeasure ,
              ItemID_SKU ,
              Description ,
              UnitCost ,
              LineTotal ,
              LineStatus
            )
            SELECT  PONUMBER ,
                    ROW_NUMBER() OVER ( PARTITION BY PONUMBER ORDER BY PONUMBER, ORD ASC ) AS ROWNUMBER ,
                    QTYORDER ,
                    UOFM ,
                    ITEMNMBR ,
                    ITEMDESC ,
                    UNITCOST ,
                    EXTDCOST ,
                    POLNESTA
            FROM    POPPOCTE;			

SELECT  poh.VENDORID ,
        poh.PONUMBER ,
        poh.DOCDATE ,
        poh.SUBTOTAL + poh.TRDISAMT + poh.FRTAMNT + poh.MSCCHAMT + poh.TAXAMNT AS TOTALDUE ,
        ISNULL(iptx.CustomerTermCode, ''NET030'') AS PYMTRMID ,
        ISNULL(curxr.ISOThree, ''USD'') AS CURNCYID ,
        shipto.LOCATNNM AS SHIPTO_NAME ,
        shipto.ADDRESS1 AS SHIPTO_ADDRESS1 ,
        shipto.ADDRESS2 AS SHIPTO_ADDRESS2 ,
        shipto.CITY AS SHIPTO_CITY ,
        shipto.STATE AS SHIPTO_STATE ,
        shipto.ZIPCODE AS SHIPTO_ZIPCODE ,
        shipto.COUNTRY AS SHIPTO_COUNTRY ,
        '' AS SHIPTO_PHONE1 ,
        '' AS SHIPTO_PHONE2 ,
        billto.LOCATNNM AS BILLTO_NAME ,
        billto.ADDRESS1 AS BILLTO_ADDRESS1 ,
        billto.CITY AS BILLTO_CITY ,
        billto.STATE AS BILLTO_STATE ,
        billto.ZIPCODE AS BILLTO_ZIPCODE ,
        ISNULL(cxr.ISOTwo, ''US'') AS BILLTO_COUNTRY  -- comes from exteral table
        ,
        billto.PHONE1 AS BILLTO_PHONE1 ,
        billto.PHONE2 AS BILLTO_PHONE2 ,
        CASE POSTATUS
          WHEN 1 THEN ''OPEN''
          WHEN 2 THEN ''OPEN''
          WHEN 3 THEN ''OPEN''
          WHEN 4 THEN ''CLSD''
          WHEN 5 THEN ''CLSD''
          WHEN 6 THEN ''CLSD''
          ELSE ''OPEN''
        END AS STATUS ,
        pol.POLineNbr ,
        pol.Quantity AS Quantity ,
        pol.UnitOfMeasure ,
        pol.ItemID_SKU ,
        pol.Description ,
        pol.UnitCost AS UnitCost ,
        pol.LineTotal AS LineTotal ,
        poh.POTYPE AS POTYPE ,
        '' AS HCF3 , 
        '' AS HCF1 ,
        '' LCF1 ,
        '' AS LCF2 ,
        '' AS LCF3 ,
        '' AS LCF4 ,
        0 AS QuantityReceived ,
        CASE LineStatus
          WHEN 1 THEN ''OPEN''
          WHEN 2 THEN ''OPEN''
          WHEN 3 THEN ''OPEN''
          WHEN 4 THEN ''CLSD''
          WHEN 5 THEN ''CLSD''
          WHEN 6 THEN ''CLSD''
          ELSE ''OPEN''
        END AS LINESTATUS
FROM    ' + @databaseName + '.dbo.POP10100 poh --LEFT OUTER JOIN FADGP.dbo.SY00600 shipto ON poh.PRSTADCD = shipto.LOCATNID
        LEFT OUTER JOIN ' + @databaseName + '.dbo.SY00600 billto ON poh.PRBTADCD = billto.LOCATNID
        LEFT OUTER JOIN ' + @databaseName + '.dbo.SY00600 shipto ON poh.PRSTADCD = shipto.LOCATNID
        JOIN @poLine pol ON poh.PONUMBER = pol.PONbr
        LEFT OUTER JOIN CountryXRef cxr ON billto.COUNTRY = cxr.GPCountry
        --LEFT OUTER JOIN FADGP.dbo.FAD_IPAYABLES fadip ON poh.PONUMBER = fadip.PONUMBER
        --                                                 AND pol.PONbr = fadip.PONUMBER
        --                                                 AND pol.POLineNbr = fadip.linenumber
        --LEFT OUTER JOIN @FADPOTotal fadpot ON fadpot.PONbr = poh.PONUMBER
        --LEFT OUTER JOIN @classLookup cl ON fadip.po_class = Code
        LEFT OUTER JOIN iPayablesTermXRef iptx ON poh.PYMTRMID = iptx.GPTermID
        LEFT OUTER JOIN CurrencyXRef curxr ON poh.CURNCYID = curxr.GPCurrencyID;
--WHERE poh.MODIFDT > DATEADD(DAY,-3, GETUTCDATE())'


        exec (@sql)

GO
