﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using IWshRuntimeLibrary;
using System.Management;

namespace DM_Helpers
{
    public class InstallHelper
    {

        public static string EncryptString(string ClearText, string First, string Second)
        {

            byte[] clearTextBytes = Encoding.UTF8.GetBytes(ClearText);

            System.Security.Cryptography.SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();

            MemoryStream ms = new MemoryStream();
            byte[] rgbIV = Encoding.ASCII.GetBytes(First);
            byte[] key = Encoding.ASCII.GetBytes(Second);
            CryptoStream cs = new CryptoStream(ms, rijn.CreateEncryptor(key, rgbIV),
            CryptoStreamMode.Write);

            cs.Write(clearTextBytes, 0, clearTextBytes.Length);

            cs.Close();

            return Convert.ToBase64String(ms.ToArray());
        }


        public static string DecryptString(string EncryptedText, string First, string Second)
        {
            byte[] encryptedTextBytes = Convert.FromBase64String(EncryptedText);

            MemoryStream ms = new MemoryStream();

            System.Security.Cryptography.SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();


            byte[] rgbIV = Encoding.ASCII.GetBytes(First);
            byte[] key = Encoding.ASCII.GetBytes(Second);

            CryptoStream cs = new CryptoStream(ms, rijn.CreateDecryptor(key, rgbIV),
            CryptoStreamMode.Write);

            cs.Write(encryptedTextBytes, 0, encryptedTextBytes.Length);

            cs.Close();

            return Encoding.UTF8.GetString(ms.ToArray());

        }

        public static string CreateDirectory(string targetPath)
        {
            try
            {
                // Create a new target folder, if necessary. 
                if (!System.IO.Directory.Exists(targetPath))
                {
                    System.IO.Directory.CreateDirectory(targetPath);
                }
                return "true";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }


        public static string CopyFiles(string[] Params, string Source, string Target)
        {
            string sourcePath = Source;
            string targetPath = Target;

            if (System.IO.Directory.Exists(sourcePath))
            {
                try
                {
                    foreach (string str in Params)
                    {
                        // To copy a folder's contents to a new location: 
                        // Create a new target folder, if necessary. 
                        if (!System.IO.Directory.Exists(targetPath))
                        {
                            System.IO.Directory.CreateDirectory(targetPath);
                        }

                        //get a list of all files in source directory with specified parameters
                        List<string> files = new List<string>();
                        foreach (string p in Params)
                            files.AddRange(System.IO.Directory.GetFiles(sourcePath, p, SearchOption.TopDirectoryOnly));
                        files.Sort();

                        // Copy the files and overwrite destination files if they already exist. 
                        foreach (string s in files)
                        {
                            // Use static Path methods to extract only the file name from the path.
                            string fileName = System.IO.Path.GetFileName(s);
                            string destFile = System.IO.Path.Combine(targetPath, fileName);
                            System.IO.File.Copy(s, destFile, true);
                        }

                    }
                    return "true";
                }
                catch (Exception Ex)
                {
                    return Ex.ToString();
                }
            }
            else
            {
                //error, source path does not exist
                return "Source directory " + sourcePath + " does not exist!";
            }
        }

        public static void DeleteFile(string FileName)
        {
            if (System.IO.File.Exists(FileName))
            {
                System.IO.File.Delete(FileName);
            }
        }

        public static string UpdateLogFile(string FileName, string Message)
        {
            //try
            //{
                //use current directory
                //string PathString = System.IO.Directory.GetCurrentDirectory();
                //string FileName = "iPayInstallationLog.txt";

                // Make sure the path exists
                string folder = Path.GetDirectoryName(FileName);
                if (Directory.Exists(folder)==false){
                    Directory.CreateDirectory(folder);
                }

                // Check if file already exists. If not, create it. 
                if (!System.IO.File.Exists(FileName))
                {
                    // Create a new file 
                    FileStream fs = System.IO.File.Create(FileName);
                    fs.Close(); //close file

                    //Write data to data file
                    System.IO.File.AppendAllText(FileName, DateTime.Now.ToString() + ": File Created." + Environment.NewLine);
                }

                //Write message to log file
                System.IO.File.AppendAllText(FileName, DateTime.Now.ToString() + ": " + Message + Environment.NewLine);

                return ("true");
            //}
            //catch (Exception Ex)
            //{
            //    return (Ex.ToString());
            //}
        }

        public static string FileToString(string FileName)
        {
            // Check if file already exists. If not, create it. 
            if (System.IO.File.Exists(FileName))
            {
                return System.IO.File.ReadAllText(FileName);
            }
            else return "";
        }

        public static string CreateDataFile(string FileName, string Path, string[] Data)
        {
            try
            {
                //use current directory
                string PathString = Path;

                // Create a new target folder, if necessary. 
                if (!System.IO.Directory.Exists(Path))
                {
                    System.IO.Directory.CreateDirectory(Path);
                }

                // Check if file already exists. If not, create it. 
                if (System.IO.File.Exists(FileName))
                {
                    //delete the old file
                    System.IO.File.Delete(FileName);   
                }

                // Create a new file 
                FileStream fs = System.IO.File.Create(FileName);
                fs.Close(); //close file

                //Write data to data file
                foreach (string str in Data)
                {
                    //Write data to data file
                    System.IO.File.AppendAllText(FileName, str + Environment.NewLine);
                }

                return ("true");
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public static bool testConnection(string conString)
        {
            using (SqlConnection con = new SqlConnection(conString))
            {
                try
                {
                    con.Open();
                    con.Close();
                    return true;
                }
                catch (Exception)
                {
                    //can't connect
                    return false;
                }
            }
        }

        public static string CreateRegistryKey(string AppName, string KeyName, string KeyValue)
        {
            try
            {
                RegistryKey MyReg = Registry.LocalMachine.CreateSubKey
                     ("SOFTWARE\\" + AppName);
                MyReg.SetValue(KeyName, KeyValue);
                MyReg.Close();

                return "true";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public static string GetOperatingSystem()
        {
            //get os version
            string result = string.Empty;
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT Caption FROM Win32_OperatingSystem");
            foreach (ManagementObject os in searcher.Get())
            {
                result = os["Caption"].ToString();
                break;
            }

            return result;
        }

       //public static void CreateStartMenuShortcut(string directoryName, string shortcutName, string description, string exePath)
       // {
       //     string startDir = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu) + @"\Programs\" + directoryName + "\\";;

       //     // Create a new target folder, if necessary. 
       //     if (!System.IO.Directory.Exists(startDir))
       //     {
       //         System.IO.Directory.CreateDirectory(startDir);
       //     }

       //     IWshShortcut MyShortcut;
       //     WshShell shell = new WshShell();
       //     // Replace [UserName] with the name of the User
       //     MyShortcut = (IWshShortcut)shell.CreateShortcut(startDir + shortcutName + ".lnk");
       //     MyShortcut.TargetPath = exePath;
       //     MyShortcut.Description = description;
       //     // MyShortcut.IconLocation = [IconPath]; // [IconPath] - the path of the icon for the shortcut, if you have one 
       //     MyShortcut.Save();
       // }

       //public static void CreateShortcut(string targetDirectory, string shortcutName, string description, string arguments, string exePath)
       //{
       //    // Create a new target folder, if necessary. 
       //    if (!System.IO.Directory.Exists(targetDirectory))
       //    {
       //        System.IO.Directory.CreateDirectory(targetDirectory);
       //    }

       //    IWshShortcut MyShortcut;
       //    WshShell shell = new WshShell();
       //    // Replace [UserName] with the name of the User
       //    //MyShortcut = (IWshShortcut)shell.CreateShortcut(targetDirectory + shortcutName + ".lnk");
       //    MyShortcut = (IWshShortcut)shell.CreateShortcut(targetDirectory + shortcutName + ".lnk");
       //    MyShortcut.TargetPath = exePath;
       //    if(arguments.Length > 0) MyShortcut.Arguments = arguments;
       //    MyShortcut.Description = description;
       //    // MyShortcut.IconLocation = [IconPath]; // [IconPath] - the path of the icon for the shortcut, if you have one 
       //    MyShortcut.Save();
       //}



        public static void deleteFilesFolders(string path)
        {
            System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(path);

            foreach (FileInfo file in downloadedMessageInfo.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in downloadedMessageInfo.GetDirectories())
            {
                dir.Delete(true);
            }
        }
    }
}
