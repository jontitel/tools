﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;

namespace DM_Helpers
{
    public class Sql
    {
        public string Error { get; set; }
        private int UpdateCount = 0;
        private int InsertCount = 0;
        private int FailureCount = 0;

        public Sql()
        {
            Error = "";
        }

        public int GetUpdates() { return UpdateCount; }

        public int GetInserts() { return InsertCount; }

        public int GetFailures() { return FailureCount; }

        public string CreateConString(string SQLServer, string Database, string IntegratedSecurity)
        {
            string conString = "Data Source=" + SQLServer + ";Initial Catalog=" + Database + ";Integrated Security=" + IntegratedSecurity;
            //string conString = "Data Source=" + "Justin-Laptop" + ";Initial Catalog=" + "TWO" + ";Integrated Security=" + "True";
            return conString;
        }

        public List<string> GetSharedColumns(string conString1, string conString2, string Table1, string Table2)
        {
            //get shared column names from UPR00500 and UPR40900, put in Sql class?
            List<string> SharedColumns = new List<string>();
            List<string> TempList = new List<string>();
            DataTable fromSql = new DataTable();
            int doesExist = 0;
            string query;
            SqlConnection con = new SqlConnection(conString1);
            SqlConnection con2 = new SqlConnection(conString2);

            query = "SELECT TOP 1 * FROM " + con.Database + ".dbo." + Table1;

            try
            {
                fromSql = QueryDatabase(conString1, query);

                foreach (DataColumn col in fromSql.Columns)
                {
                    SharedColumns.Add(col.ToString());
                }


                foreach (string str in SharedColumns) TempList.Add(str);

                query = "SELECT TOP 1 * FROM " + con2.Database + ".dbo." + Table2;
                fromSql = QueryDatabase(conString2, query);

                foreach (string str in SharedColumns)
                {
                    foreach (DataColumn col in fromSql.Columns)
                    {
                        if (str == col.ToString()) doesExist = 1;
                    }

                    if (doesExist != 1) TempList.Remove(str);
                    doesExist = 0;
                }
            }catch(Exception ex)
            {
                Error += ex.Message + Environment.NewLine;
                //throw new Exception(ex.ToString(), ex);
            }
            return TempList;
        }

        public DataTable QueryDatabase(string conString, string Query)
        {
            DataTable dataTable = new DataTable();
            // Create a connection
            SqlConnection con = new SqlConnection(conString);

            // Open the connection
            con.Open();
            //try
            //{
                // Create a SqlCommand object and pass the constructor the connection string and the query string.
                SqlCommand queryCommand = new SqlCommand(Query, con);

                // Use the above SqlCommand object to create a SqlDataReader object.
                SqlDataReader queryCommandReader = queryCommand.ExecuteReader();

                // Use the DataTable.Load(SqlDataReader) function to put the results of the query into a DataTable.
                dataTable.Load(queryCommandReader);
            //}
            //catch (Exception ex)
            //{
                //Error += ex.Message + Environment.NewLine;
                //throw new Exception(ex.ToString(), ex);
            //}
            //finally
            //{
                // Close the connection
                con.Close();
            //}

            return dataTable;
        }

        public void Insert(string conString, string Table, string[] Column, string[] Var)
        {
            InsertCount++;

            //dynamically create query statement
            string sqlIns = "INSERT INTO " + Table + " (";

            for (int i = 0; i < Column.Length; i++)
            {
                if (i == Column.Length - 1) { sqlIns += Column[i]; }
                else { sqlIns += Column[i] + ", "; }
            }
            sqlIns += ") VALUES (";
            for (int i = 0; i < Column.Length; i++)
            {
                if (i == Var.Length - 1) { sqlIns += ("@" + Column[i]); }
                else { sqlIns += ("@" + Column[i] + ", "); }
            }
            sqlIns += ")";
            //end create query statement

            SqlConnection con = new SqlConnection(conString);

            con.Open();
            try
            {
                SqlCommand cmdIns = new SqlCommand(sqlIns, con);

                //create and insert parameters
                for (int i = 0; i < Var.Length; i++)
                {
                    SqlParameter param = new SqlParameter("@" + Column[i], Var[i]);
                    cmdIns.Parameters.Add(param);
                }

                cmdIns.ExecuteNonQuery();
                cmdIns.Dispose();
                cmdIns = null;
            }
            catch (Exception ex)
            {
                Error += ex.Message + Environment.NewLine;
                FailureCount++;
                InsertCount--;
                //throw new Exception(ex.ToString(), ex);
            }
            finally
            {
                con.Close();
            }
        }

        public void Update(string conString, string Table, string[] Column, string[] Var, string[] Where, string[] What)
        {
            UpdateCount = UpdateCount + 1;

            //dynamically create query statement
            string sqlIns = "UPDATE " + Table + " SET ";

            for (int i = 0; i < Column.Length; i++)
            {
                if (i == Column.Length - 1) { sqlIns += Column[i] + " = @" + Column[i]; }
                else { sqlIns += Column[i] + " = @" + Column[i] + ", "; }
            }

            sqlIns += " WHERE " + Where[0] + "= @" + Where[0];

            if (Where.Length > 1)
            {
                for (int i = 1; i < Where.Length; i++)
                {
                    sqlIns += " AND " + Where[i] + "= @" + Where[i];
                }
            }
            //end query statement

            SqlConnection con = new SqlConnection(conString);
            con.Open();
            try
            {
                SqlCommand cmdIns = new SqlCommand(sqlIns, con);

                //create and insert parameters
                for (int i = 0; i < Var.Length; i++)
                {
                    SqlParameter param = new SqlParameter("@" + Column[i], Var[i]);
                    cmdIns.Parameters.Add(param);
                }
                for (int i = 0; i < Where.Length; i++)
                {
                    SqlParameter param2 = new SqlParameter("@" + Where[i], What[i]);
                    cmdIns.Parameters.Add(param2);
                }

                cmdIns.ExecuteNonQuery();
                cmdIns.Dispose();
                cmdIns = null;
            }
            catch (Exception ex)
            {
                Error += ex.Message + Environment.NewLine;
                FailureCount ++;
                UpdateCount--;
                //throw new Exception(ex.ToString(), ex);
            }
            finally
            {
                con.Close();
            }
        }

    }
}
