﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Net;
using System.IO;
using System.Data.SqlClient;
using DM_Helpers;
using ImportApplication;
using Microsoft.Win32;
using System.Collections.Specialized;

namespace iPayables_Integration
{
    class iPayables_Helpers
    {
        private string AccountFileLocation = "";
        private string VendorFileLocation = "";
        private string InvoiceFileLocation = "";

        public static string GetDynamicsConnectionString()
        {
            RegistryKey reg = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Invoiceworks", true);

            string GPServer = reg.GetValue("GPServer").ToString();
            string DynamicsDB = reg.GetValue("DynamicsDB").ToString();

            string conString = "data source=" + GPServer +
                ";initial catalog=" + DynamicsDB +
                ";integrated security=SSPI;persist security info=False;packet size=4096";

            return conString;
        }

        public string ExportGLAccounts(DataTable dta, string CustId, string CompanyID, NameValueCollection AppSettings, string TimeStamp)
        {
            string fileName = AppSettings["CodeExportMask"].Replace("[TS]", TimeStamp);
            if (fileName.IndexOf("[CID]") > -1)
            {
                fileName = fileName.Replace("[CID]", CompanyID);
            }

            string AccountFileLocation = AppSettings["ExportFolder"] +  fileName;
           
            XmlWriter writer = XmlWriter.Create(AccountFileLocation);
            writer.WriteStartDocument();
            writer.WriteStartElement("InvoiceWorksInterface_Codes");
            writer.WriteAttributeString("InvoiceWorksCustomerID", CustId);
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            foreach (DataRow r in dta.Rows)
            {
                writer.WriteStartElement("Code");
                //---------------------------------------------
                writer.WriteStartElement("BusinessUnit");
                writer.WriteString(CompanyID);
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("Level");
                writer.WriteString("D");
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("CFNbr");
                writer.WriteString("1");
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("Value");
                writer.WriteString(r["ACTNUMST"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("Description");
                writer.WriteString(r["ACTDESCR"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------               
                writer.WriteEndElement();
            }
            writer.WriteStartElement("Audit");
            //---------------------------------------------
            writer.WriteStartElement("TotalCodesCount");
            writer.WriteString(dta.Rows.Count.ToString());
            writer.WriteEndElement();
            //---------------------------------------------
            writer.WriteEndElement();
            writer.Close();

            return AccountFileLocation;
        }

        public string ExportVendors(DataTable dtv, string CustId, string CompanyID, string CompanyName, NameValueCollection AppSettings, string TimeStamp)
        {
            
            string fileName = AppSettings["VendorExportMask"].Replace("[TS]", TimeStamp);
            if (fileName.IndexOf("[CID]") > -1)
            {
                fileName = fileName.Replace("[CID]", CompanyID);
            }

            VendorFileLocation = AppSettings["ExportFolder"] +  fileName;

            XmlWriter writer = XmlWriter.Create(VendorFileLocation);
            writer.WriteStartDocument();
            writer.WriteStartElement("InvoiceWorksInterface_Vendors");
            writer.WriteAttributeString("InvoiceWorksCustomerID", CustId);
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");

            // Check if BU is to be defaulted or derived from VendorID
            bool deriveBU;
            int buStartPos = 0;
            int buLength = 0;
            if (string.IsNullOrEmpty(AppSettings["GetBUFromVendorID"]) == true){
                deriveBU = false;
            }
            else{
                deriveBU = Convert.ToBoolean(AppSettings["GetBUFromVendorID"]);

                if (deriveBU == true)
                {
                    if (string.IsNullOrEmpty(AppSettings["BUStartPosition"])==true){
                        // Put some exception handling here, configuration not setup properly
                        throw new System.ArgumentException("Configuration for Business Unit Derivation is not correct in Application Config","BUStartPosition");
                    }

                    if (string.IsNullOrEmpty(AppSettings["BULength"])==true){
                        throw new System.ArgumentException("Configuration for Business Unit Derivation is not correct in Application Config", "BULength");
                    }

                    buStartPos = Convert.ToInt32(AppSettings["BUStartPosition"]);
                    buLength = Convert.ToInt32(AppSettings["BULength"]);
                }
            }
            
            foreach (DataRow r in dtv.Rows)
            {
                writer.WriteStartElement("Vendor");
                //---------------------------------------------
                writer.WriteStartElement("CustomersVendorNbr");
                writer.WriteString(CompanyID + "-" + r["VENDORID"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("Name");
                writer.WriteString(Left(r["VENDNAME"].ToString(),50));
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("StreetAddress1");
                if (r["ADDRESS1"].ToString().Length > 0)
                {
                    writer.WriteString(Left(r["ADDRESS1"].ToString(), 50));
                }
                else
                {
                    writer.WriteString("MISSING");
                }

                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("StreetAddress2");
                writer.WriteString(Left(r["ADDRESS2"].ToString(),50));
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("City");
                if (r["CITY"].ToString().Length > 0)
                {
                    writer.WriteString(r["CITY"].ToString());
                }
                else
                {
                    writer.WriteString("MISSING");
                }
                writer.WriteEndElement();
                
                //---------------------------------------------
                writer.WriteStartElement("State-Region");
                writer.WriteString(r["STATE"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("PostalCode");
                writer.WriteString(r["ZIPCODE"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("Country");
                if (r["COUNTRY"].ToString() == string.Empty || r["COUNTRY"].ToString().Length > 2)
                {
                    writer.WriteString("US");
                }
                else
                {
                    writer.WriteString(r["COUNTRY"].ToString());
                }
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("ContactName");
                writer.WriteString(Left(r["VNDCNTCT"].ToString(),50));
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("ContactPhone");
                writer.WriteString(r["PHNUMBR1"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("ContactFax");
                writer.WriteString(r["FAXNUMBR"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("ContactEmail");
                writer.WriteString(r["VENDOREMAIL"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("BusinessUnit");
                //---------------------------------------------
                writer.WriteStartElement("BusinessUnitID");
                if (deriveBU == true)
                {
                    writer.WriteString(r["VENDORID"].ToString().Substring(buStartPos,buLength));
                }
                else
                {
                    writer.WriteString(CompanyID);
                }
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("BusinessUnitName");
                writer.WriteString(CompanyName);
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("Status");
                writer.WriteString("ACTV");
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("PaymentMethod");
                writer.WriteString(r["PAYMENTMETHOD"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------

                if (r["PAYMENTTERMS"].ToString().Length > 0)
                {
                    writer.WriteStartElement("PaymentTerm");
                    writer.WriteString(r["PAYMENTTERMS"].ToString());
                    writer.WriteEndElement();
                }
    
                //---------------------------------------------
                if (r["TAXIDNUMBER"].ToString().Length > 0)
                {
                    writer.WriteStartElement("AlternateID");
                    //---------------------------------------------
                    writer.WriteStartElement("AlternateIDType");
                    writer.WriteString("EIN");
                    writer.WriteEndElement();
                    //---------------------------------------------
                    writer.WriteStartElement("AlternateIDNbr");
                    writer.WriteString(r["TAXIDNUMBER"].ToString());
                    writer.WriteEndElement();
                    //---------------------------------------------
                    writer.WriteEndElement();
                }
                //---------------------------------------------
                if (r["APACCTNUM"].ToString().Length > 0)
                {
                    writer.WriteStartElement("AlternateID");
                    //---------------------------------------------
                    writer.WriteStartElement("AlternateIDType");
                    writer.WriteString("ALT");
                    writer.WriteEndElement();
                    //---------------------------------------------
                    writer.WriteStartElement("AlternateIDNbr");
                    writer.WriteString(r["APACCTNUM"].ToString());
                    writer.WriteEndElement();
                    //---------------------------------------------
                    writer.WriteEndElement();
                }
                //---------------------------------------------
                if (r["PRCHACCTNUM"].ToString().Length > 0)
                {
                    writer.WriteStartElement("VendorCustomFields");
                    //---------------------------------------------
                    writer.WriteStartElement("CFNbr");
                    writer.WriteString("1");
                    writer.WriteEndElement();
                    //---------------------------------------------
                    writer.WriteStartElement("Value");
                    writer.WriteString(r["PRCHACCTNUM"].ToString());
                    writer.WriteEndElement();
                    //---------------------------------------------
                    writer.WriteStartElement("Description");
                    writer.WriteString(r["PRCHACCTDESC"].ToString());
                    writer.WriteEndElement();
                    //---------------------------------------------              
                    writer.WriteEndElement();
                }
                //---------------------------------------------
                if (r["APACCTNUM"].ToString().Length > 0)
                {
                    writer.WriteStartElement("VendorCustomFields");
                    //---------------------------------------------
                    writer.WriteStartElement("CFNbr");
                    writer.WriteString("2");
                    writer.WriteEndElement();
                    //---------------------------------------------
                    writer.WriteStartElement("Value");
                    writer.WriteString(r["APACCTNUM"].ToString());
                    writer.WriteEndElement();
                    //---------------------------------------------
                    writer.WriteStartElement("Description");
                    writer.WriteString(r["APACCTDESC"].ToString());
                    writer.WriteEndElement();
                    //---------------------------------------------              
                    writer.WriteEndElement();
                }
                //---------------------------------------------
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteEndElement();
            }
            writer.WriteStartElement("Audit");
            //---------------------------------------------
            writer.WriteStartElement("VendorCount");
            writer.WriteString(dtv.Rows.Count.ToString());
            writer.WriteEndElement();
            //---------------------------------------------
            writer.WriteStartElement("BusinessUnitCount");
            writer.WriteString("1");
            writer.WriteEndElement();
            //---------------------------------------------
            writer.WriteEndElement();

            writer.Close();

            return VendorFileLocation;
        }
        
        public string ExportPurchaseOrders(DataTable dtp, string CustId, string CompanyID, string CompanyName, NameValueCollection AppSettings,string TimeStamp)
        {
            // ********************  CHANGE LOG   ***********************
            // *** 2/20/2015  JT  Added setting of PO and PO Line Status using data from SQL Lookup
            // ********************  CHANGE LOG   ***********************

            string fileName = AppSettings["POExportMask"].Replace("[TS]", TimeStamp);
            if (fileName.IndexOf("[CID]") > -1)
            {
                fileName = fileName.Replace("[CID]", CompanyID);
            }

            string POFileLocation = AppSettings["ExportFolder"] + fileName;
            XmlWriter writer = XmlWriter.Create(POFileLocation);
            writer.WriteStartDocument();
            writer.WriteStartElement("InvoiceWorksInterface_PurchaseOrders");
            writer.WriteAttributeString("InvoiceWorksCustomerID", CustId);
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");

            decimal auditPOTotal = 0;
            int auditPOCount = 0;

            // Check if BU is to be defaulted or derived from VendorID
            bool deriveBU;
            int buStartPos = 0;
            int buLength = 0;
            if (string.IsNullOrEmpty(AppSettings["GetBUFromVendorID"]) == true)
            {
                deriveBU = false;
            }
            else
            {
                deriveBU = Convert.ToBoolean(AppSettings["GetBUFromVendorID"]);

                if (deriveBU == true)
                {
                    if (string.IsNullOrEmpty(AppSettings["BUStartPosition"]) == true)
                    {
                        // Put some exception handling here, configuration not setup properly
                        throw new System.ArgumentException("Configuration for Business Unit Derivation is not correct in Application Config", "BUStartPosition");
                    }

                    if (string.IsNullOrEmpty(AppSettings["BULength"]) == true)
                    {
                        throw new System.ArgumentException("Configuration for Business Unit Derivation is not correct in Application Config", "BULength");
                    }

                    buStartPos = Convert.ToInt32(AppSettings["BUStartPosition"]);
                    buLength = Convert.ToInt32(AppSettings["BULength"]);
                }
            }

            string PONbr="";
            foreach (DataRow r in dtp.Rows)
            {
                if (PONbr != Left(r["PONUMBER"].ToString(), 50))
                {
                    if (PONbr != "")
                    {
                        PONbr = Left(r["PONUMBER"].ToString(), 50);
                        writer.WriteEndElement();
                    }
                    else { PONbr = Left(r["PONUMBER"].ToString(), 50); }
                    auditPOCount++;
                    writer.WriteStartElement("PurchaseOrder");
                    //---------------------------------------------
                    writer.WriteStartElement("CustomersVendorNbr");
                    writer.WriteString(CompanyID + "-" + r["VENDORID"].ToString());
                    writer.WriteEndElement();
                    //---------------------------------------------
                    writer.WriteStartElement("PurchaseOrderNbr");
                    writer.WriteString(Left(r["PONUMBER"].ToString(), 50));
                    writer.WriteEndElement();

                    writer.WriteStartElement("PurchaseOrderDate");
                    DateTime PODate = Convert.ToDateTime(r["DOCDATE"].ToString());
                    writer.WriteString(PODate.ToString("yyyy-MM-dd"));
                    writer.WriteEndElement();

                    writer.WriteStartElement("TotalDue");
                    decimal total = Convert.ToDecimal(r["TOTALDUE"]);
                    writer.WriteString(total.ToString("#.00"));
                    auditPOTotal += total;
                    writer.WriteEndElement();

                    writer.WriteStartElement("PaymentTerms");
                    writer.WriteString(Left(r["PYMTRMID"].ToString(), 10));
                    writer.WriteEndElement();

                    writer.WriteStartElement("Class");
                    writer.WriteString(Left(r["POTYPE"].ToString(), 50));
                    writer.WriteEndElement();

                    writer.WriteStartElement("Notes");
                    writer.WriteString("");
                    writer.WriteEndElement();

                    writer.WriteStartElement("BusinessUnitID");
                    writer.WriteString(CompanyID);
                    writer.WriteEndElement();

                    writer.WriteStartElement("Currency");
                    writer.WriteString("USD");
                    writer.WriteEndElement();

                    writer.WriteStartElement("Status");
                    writer.WriteString(Left(r["STATUS"].ToString(), 50));
                    //writer.WriteString("OPEN");
                    writer.WriteEndElement();

                    //----- Header Custom Fields
                    int idx;
                    for (idx = 1; idx < 5; idx++)
                    {
                        if (dtp.Columns.Contains("HCF" + idx.ToString()))
                        {
                            writer.WriteStartElement("CustomField");

                            writer.WriteStartElement("CFNbr");
                            writer.WriteString(idx.ToString());
                            writer.WriteEndElement();

                            writer.WriteStartElement("Value");
                            writer.WriteString(r["HCF" + idx.ToString()].ToString());
                            writer.WriteEndElement();

                            writer.WriteEndElement();
                        }
                    }
                    //----- Ship To Address
                    if (r["SHIPTO_ADDRESS1"].ToString().Length > 0)
                    {
                    writer.WriteStartElement("PurchaseOrderAddress");
                    writer.WriteStartElement("Type");
                    writer.WriteString("3");
                    writer.WriteEndElement();

                    writer.WriteStartElement("Name1");
                    writer.WriteString(Left(r["SHIPTO_NAME"].ToString(), 50));
                    writer.WriteEndElement();

                    writer.WriteStartElement("Name2");
                    writer.WriteString("");
                    writer.WriteEndElement();

                    writer.WriteStartElement("StreetAddress1");
                    writer.WriteString(Left(r["SHIPTO_ADDRESS1"].ToString(), 50));
                    writer.WriteEndElement();

                    writer.WriteStartElement("StreetAddress2");
                    writer.WriteString("");
                    writer.WriteEndElement();

                    writer.WriteStartElement("City");
                    writer.WriteString(Left(r["SHIPTO_CITY"].ToString(), 50));
                    writer.WriteEndElement();

                    writer.WriteStartElement("StateRegion");
                    writer.WriteString(Left(r["SHIPTO_STATE"].ToString(), 50));
                    writer.WriteEndElement();

                    writer.WriteStartElement("ZipCode");
                    writer.WriteString(Left(r["SHIPTO_ZIPCODE"].ToString(), 50));
                    writer.WriteEndElement();

                    writer.WriteStartElement("Country");
                    writer.WriteString("US");
                    writer.WriteEndElement();

                    writer.WriteStartElement("Telephone1");
                    writer.WriteString(Left(r["SHIPTO_PHONE1"].ToString(), 20));
                    writer.WriteEndElement();

                    writer.WriteStartElement("Telephone2");
                    writer.WriteString(Left(r["SHIPTO_PHONE2"].ToString(), 20));
                    writer.WriteEndElement();

                    writer.WriteStartElement("ContactName");
                    writer.WriteString("");
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                    }
                    //----- Ship To Address

                    //----- Bill To Address
                    writer.WriteStartElement("PurchaseOrderAddress");
                    writer.WriteStartElement("Type");
                    writer.WriteString("4");
                    writer.WriteEndElement();

                    writer.WriteStartElement("Name1");
                    writer.WriteString(Left(r["BILLTO_NAME"].ToString(), 50));
                    writer.WriteEndElement();

                    writer.WriteStartElement("Name2");
                    writer.WriteString("");
                    writer.WriteEndElement();

                    writer.WriteStartElement("StreetAddress1");
                    writer.WriteString(Left(r["BILLTO_ADDRESS1"].ToString(), 50));
                    writer.WriteEndElement();

                    writer.WriteStartElement("StreetAddress2");
                    writer.WriteString("");
                    writer.WriteEndElement();

                    writer.WriteStartElement("City");
                    writer.WriteString(Left(r["BILLTO_CITY"].ToString(), 50));
                    writer.WriteEndElement();

                    writer.WriteStartElement("StateRegion");
                    writer.WriteString(Left(r["BILLTO_STATE"].ToString(), 50));
                    writer.WriteEndElement();

                    writer.WriteStartElement("ZipCode");
                    writer.WriteString(Left(r["BILLTO_ZIPCODE"].ToString(), 50));
                    writer.WriteEndElement();

                    writer.WriteStartElement("Country");
                    writer.WriteString("US");
                    writer.WriteEndElement();

                    writer.WriteStartElement("Telephone1");
                    writer.WriteString(Left(r["BILLTO_PHONE1"].ToString(), 20));
                    writer.WriteEndElement();

                    writer.WriteStartElement("Telephone2");
                    writer.WriteString(Left(r["BILLTO_PHONE2"].ToString(), 20));
                    writer.WriteEndElement();

                    writer.WriteStartElement("ContactName");
                    writer.WriteString("");
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                    //----- Bill To Address
                }

                //----- Purchase Order Line
                writer.WriteStartElement("PurchaseOrderLine");

                writer.WriteStartElement("LineNbr");
                writer.WriteString(r["POLineNbr"].ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("Quantity");
                writer.WriteString(r["Quantity"].ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("UnitOfMeasure");
                writer.WriteString(r["UnitOfMeasure"].ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("ItemID_SKU");
                writer.WriteString(r["ItemID_SKU"].ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("Description");
                writer.WriteString(r["Description"].ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("UnitCost");
                writer.WriteString(r["UnitCost"].ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("LineTotal");
                writer.WriteString(r["LineTotal"].ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("QuantityReceived");
                writer.WriteString(r["QuantityReceived"].ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("QuantityInvoiced");
                writer.WriteString("0");
                writer.WriteEndElement();

                writer.WriteStartElement("AmountInvoiced");
                writer.WriteString("0");
                writer.WriteEndElement();

                writer.WriteStartElement("Status");
                writer.WriteString(Left(r["LINESTATUS"].ToString(), 50));
                //writer.WriteString("OPEN");
                writer.WriteEndElement();

                writer.WriteStartElement("CommodityCode");
                writer.WriteString("");
                writer.WriteEndElement();

                //----- Line Custom Fields
                int lIdx;
                for (lIdx = 1; lIdx < 5; lIdx++)
                {
                    if (dtp.Columns.Contains("LCF" + lIdx.ToString()))
                    {
                        writer.WriteStartElement("LineCustomField");

                        writer.WriteStartElement("CFNbr");
                        writer.WriteString(lIdx.ToString());
                        writer.WriteEndElement();

                        writer.WriteStartElement("Value");
                        writer.WriteString(r["LCF" + lIdx.ToString()].ToString());
                        writer.WriteEndElement();

                        writer.WriteEndElement();
                    }
                }

                writer.WriteEndElement();
                //----- Purchase Order Line


            }
            writer.WriteEndElement();
            // Purchase Order
            //Audit
            writer.WriteStartElement("Audit");
            //---------------------------------------------
            writer.WriteStartElement("TotalPurchaseOrderCount");
            writer.WriteString(auditPOCount.ToString());
            writer.WriteEndElement();
            //---------------------------------------------
            writer.WriteStartElement("TotalPurchaseOrderAmount");
            writer.WriteString(auditPOTotal.ToString());
            auditPOTotal = 0;
            writer.WriteEndElement();
            //---------------------------------------------
            writer.WriteEndElement();
            writer.Close();

            return POFileLocation;
        }

        //export GL invoices
        //use same structure as email, logic as other two exports
        //(CASHC stored procedure DMGetInvoice for field names)
        public string ExportGLInvoices(DataTable dti, string CustId, string CompanyID, NameValueCollection AppSettings,string TimeStamp)
        {
            decimal invoiceAmountTotal = 0;

            string fileName = AppSettings["StatusExportMask"].Replace("[TS]", TimeStamp);
            if (fileName.IndexOf("[CID]") > -1)
            {
                fileName = fileName.Replace("[CID]", CompanyID);
            }

            string InvoiceFileLocation = AppSettings["ExportFolder"] + fileName;
            XmlWriter writer = XmlWriter.Create(InvoiceFileLocation);
            writer.WriteStartDocument();
            writer.WriteStartElement("InvoiceWorksStandardInterface_InvoiceStatus");
            writer.WriteAttributeString("InvoiceWorksCustomerID", CustId);
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            //writer.WriteAttributeString("xsi", "schemaLocation", null, "http://dm.com/iPayables InvoiceWorksInterface_Invoice.xsd");

            // Check if BU is to be defaulted or derived from VendorID
            bool deriveBU;
            int buStartPos = 0;
            int buLength = 0;
            if (string.IsNullOrEmpty(AppSettings["GetBUFromVendorID"]) == true)
            {
                deriveBU = false;
            }
            else
            {
                deriveBU = Convert.ToBoolean(AppSettings["GetBUFromVendorID"]);

                if (deriveBU == true)
                {
                    if (string.IsNullOrEmpty(AppSettings["BUStartPosition"]) == true)
                    {
                        // Put some exception handling here, configuration not setup properly
                        throw new System.ArgumentException("Configuration for Business Unit Derivation is not correct in Application Config", "BUStartPosition");
                    }

                    if (string.IsNullOrEmpty(AppSettings["BULength"]) == true)
                    {
                        throw new System.ArgumentException("Configuration for Business Unit Derivation is not correct in Application Config", "BULength");
                    }

                    buStartPos = Convert.ToInt32(AppSettings["BUStartPosition"]);
                    buLength = Convert.ToInt32(AppSettings["BULength"]);
                }
            }

            foreach (DataRow r in dti.Rows)
            {
                writer.WriteStartElement("Invoice");
                //---------------------------------------------
                writer.WriteStartElement("CustomersVendorNbr");
                writer.WriteString(CompanyID + "-" + r["CustomersVendorNbr"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("BusinessUnitID");
                if (deriveBU == true)
                {
                    writer.WriteString(r["CustomersVendorNbr"].ToString().Substring(buStartPos, buLength));
                }
                else
                {
                    writer.WriteString(CompanyID);
                }
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("InvoiceNbr");
                writer.WriteString(r["InvoiceNbr"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("InvoiceDate");
                DateTime InvoiceDate = Convert.ToDateTime(r["InvoiceDate"].ToString());
                writer.WriteString(InvoiceDate.ToString("yyyy-MM-dd"));
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("VoucherNbr");
                writer.WriteString(r["VoucherNbr"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("PONbr");
                writer.WriteString(r["PONbr"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("InvoiceDueDate");
                DateTime InvoiceDueDate = Convert.ToDateTime(r["InvoiceDueDate"].ToString());
                writer.WriteString(InvoiceDueDate.ToString("yyyy-MM-dd"));
                writer.WriteEndElement();
                //---------------------------------------------  
                writer.WriteStartElement("PaymentReferenceNbr");
                writer.WriteString(r["PaymentReferenceNbr"].ToString());
                writer.WriteEndElement();
                //--------------------------------------------- 
                writer.WriteStartElement("PaymentDate");
                DateTime PaymentDate = Convert.ToDateTime(r["PaymentDate"].ToString());
                writer.WriteString(PaymentDate.ToString("yyyy-MM-dd"));
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("PaymentType");
                writer.WriteString(r["PaymentType"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("InvoiceAmount");
                writer.WriteString(r["InvoiceAmount"].ToString());
                invoiceAmountTotal += Convert.ToDecimal(r["InvoiceAmount"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("DiscountAmount");
                writer.WriteString(r["DiscountAmount"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("NetAmount");
                writer.WriteString(r["NetAmount"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("PaymentAmount");
                writer.WriteString(r["PaymentAmount"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("Currency");
                string Currency = r["Currency"].ToString().Trim();
                if (Currency.Length > 0)
                {
                    if (Currency != "USD")
                    {
                        Currency = "USD";
                    }
                   }
                else
                {
                    Currency = "USD";
                }
                writer.WriteString(Currency);
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteStartElement("Status");
                writer.WriteString(r["Status"].ToString());
                writer.WriteEndElement();
                //---------------------------------------------
                writer.WriteEndElement();
            }
            writer.WriteStartElement("Audit");
            //---------------------------------------------
            writer.WriteStartElement("InvoiceCount");
            writer.WriteString(dti.Rows.Count.ToString());
            writer.WriteEndElement();
            //---------------------------------------------
            writer.WriteStartElement("InvoiceAmountTotal");
            writer.WriteString(invoiceAmountTotal.ToString());
            invoiceAmountTotal = 0;
            writer.WriteEndElement();
            //---------------------------------------------
            writer.WriteEndElement();
            writer.Close();

            return InvoiceFileLocation; 
        }

        public List<string> GetDirectoryList(string uri, ICredentials creds)
        {
            List<string> files = new List<string>();
            FtpWebRequest ftp = (FtpWebRequest)WebRequest.Create(uri);
            ftp.Credentials = creds;
            ftp.Method = WebRequestMethods.Ftp.ListDirectory;
            FtpWebResponse response = (FtpWebResponse)ftp.GetResponse();
            StreamReader stream = new StreamReader(response.GetResponseStream());
            string line = stream.ReadLine();
            while (!string.IsNullOrEmpty(line))
            {
                files.Add(line);
                line = stream.ReadLine();
            }
            stream.Close();
            response.Close();
            return files;
        }

        //public void GetFiles(string uri, ICredentials creds, List<string> files, string InstallationDirectory)
        //{
        //    foreach (string file in files)
        //    {
        //        FtpWebRequest ftp = (FtpWebRequest)WebRequest.Create(uri + file);
        //        ftp.Credentials = creds;
        //        ftp.Method = WebRequestMethods.Ftp.DownloadFile;
        //        FtpWebResponse response = (FtpWebResponse)ftp.GetResponse();
        //        Stream responseStream = response.GetResponseStream();
        //        using (Stream s = File.Create(InstallationDirectory + @"\Import\" + file))
        //        {
        //            responseStream.CopyTo(s);
        //        }
        //        response.Close();


        //        ftp = (FtpWebRequest)WebRequest.Create(uri + file);
        //        ftp.Credentials = creds;
        //        ftp.Method = WebRequestMethods.Ftp.DeleteFile;
        //        response = (FtpWebResponse)ftp.GetResponse();
        //        response.Close();
        //    }
        //}

        public void WriteFiles(string Folder, string ftpUser, string ftpPassword, int fileType, string vSourceFileName)
        {
            //NetworkCredential creds = new NetworkCredential(ftpUser, ftpPassword);
            //string vCustNumber = ftpUser.Substring(2, ftpUser.Length - 2);
            //string vFileName = "";

            //switch (fileType)
            //{
            //    //1. Vendor
            //    case 1:
            //        vFileName = "IP" + vCustNumber + "_Vendor_" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xml"; break;
            //    //2. Status
            //    case 2:
            //        vFileName = "IP" + vCustNumber + "_Status_" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xml"; break;
            //    //3. Codes                
            //    case 3:
            //        vFileName = "IP" + vCustNumber + "_Codes_" + DateTime.Now.ToString("ddMMyyyyhhmm") + ".xml"; break;
            //    //4. ErrorLog
            //    case 4:
            //        vFileName = Path.GetFileName(vSourceFileName); break;
            //}

            //StreamReader sourceStream = new StreamReader(vSourceFileName);

            //byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
            //sourceStream.Close();
            //ftp.ContentLength = fileContents.Length;

            //Stream requestStream = ftp.GetRequestStream();
            //requestStream.Write(fileContents, 0, fileContents.Length);
            //requestStream.Close();

            //FtpWebResponse response = (FtpWebResponse)ftp.GetResponse();
            //response.Close();
        }

        public void ExportErrorReport(string InstallationDirectory,string originalFileName, DataTable dt)
        {
            string ErrorFileLocation = InstallationDirectory + @"\Export\" + Path.GetFileNameWithoutExtension(originalFileName) + "_Result.xml";

            if(Directory.Exists(Path.GetDirectoryName(ErrorFileLocation))==false){
                Directory.CreateDirectory(Path.GetDirectoryName(ErrorFileLocation));
            }
    

            XmlWriter writer = XmlWriter.Create(ErrorFileLocation);
            writer.WriteStartDocument();
            writer.WriteStartElement("BatchInterfaceReport");
            writer.WriteStartElement("SubmissionInformation");
            //-------------------------------------------------
            writer.WriteStartElement("FileName");
            writer.WriteString(Path.GetFileName(originalFileName));
            writer.WriteEndElement();
            //-------------------------------------------------
            writer.WriteStartElement("ReceiptTS");
            writer.WriteString(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss tt"));
            writer.WriteEndElement();
            //-------------------------------------------------
            writer.WriteEndElement(); //submission information

            writer.WriteStartElement("Result");
            //-------------------------------------------------
            writer.WriteStartElement("ID");
            writer.WriteString(dt.Rows[0]["HdrID"].ToString());
            writer.WriteEndElement();
            //-------------------------------------------------
            writer.WriteStartElement("Text");
            writer.WriteString(dt.Rows[0]["HdrText"].ToString());
            writer.WriteEndElement();
            //-------------------------------------------------
            writer.WriteStartElement("ErrorKey");
            if (dt.Rows[0]["HdrErrorKey"].ToString().Length > 0) writer.WriteString(dt.Rows[0]["HdrErrorKey"].ToString());
            writer.WriteEndElement();
            //-------------------------------------------------
            writer.WriteStartElement("ErrorMessage");
            if (dt.Rows[0]["HdrErrorMsg"].ToString().Length > 0) writer.WriteString(dt.Rows[0]["HdrErrorMsg"].ToString());
            writer.WriteEndElement();
            //-------------------------------------------------
            writer.WriteEndElement(); //result

            writer.WriteStartElement("InterfaceStatistics");
            //-------------------------------------------------
            writer.WriteStartElement("TotalBusinessTransactions");
            writer.WriteString(dt.Rows[0]["TotalTransactions"].ToString());
            writer.WriteEndElement();
            //-------------------------------------------------
            writer.WriteStartElement("NewBusinessTransactions");
            writer.WriteString(dt.Rows[0]["NewTransactions"].ToString());
            writer.WriteEndElement();
            //-------------------------------------------------
            writer.WriteStartElement("FailedBusinessTransactions");
            writer.WriteString(dt.Rows[0]["FailedTransactions"].ToString());
            writer.WriteEndElement();
            //-------------------------------------------------
            writer.WriteStartElement("DuplicateBusinessTransactions");
            writer.WriteString(dt.Rows[0]["DuplicateTransactions"].ToString());
            writer.WriteEndElement();
            //-------------------------------------------------
            writer.WriteEndElement(); //interface statistics

            writer.WriteStartElement("TransactionDetail");
            if (dt.Rows[0]["ID"].ToString() != "3")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow r = dt.Rows[i];

                    writer.WriteStartElement("InvoiceStatus");
                    //--------------------------------------------------
                    writer.WriteStartElement("LineNbr");
                    writer.WriteString(r["LineNumber"].ToString());
                    writer.WriteEndElement();
                    //--------------------------------------------------
                    writer.WriteStartElement("VendorNbr");
                    writer.WriteString(r["VendorNumber"].ToString());
                    writer.WriteEndElement();
                    //--------------------------------------------------
                    writer.WriteStartElement("InvoiceNbr");
                    writer.WriteString(r["InvoiceNumber"].ToString());
                    writer.WriteEndElement();
                    //--------------------------------------------------
                    writer.WriteStartElement("InvoiceDate");
                    writer.WriteString(r["InvoiceDate"].ToString());
                    writer.WriteEndElement();
                    //--------------------------------------------------
                    writer.WriteStartElement("Total");
                    writer.WriteString(r["InvoiceTotal"].ToString());
                    writer.WriteEndElement();
                    //--------------------------------------------------

                    writer.WriteStartElement("Result");
                    //--------------------------------------------------
                    writer.WriteStartElement("ID");
                    writer.WriteString(r["ID"].ToString());
                    writer.WriteEndElement();
                    //--------------------------------------------------
                    writer.WriteStartElement("Text");
                    writer.WriteString(r["Text"].ToString());
                    writer.WriteEndElement();
                    //--------------------------------------------------
                    writer.WriteStartElement("ErrorKey");
                    if (r["ErrorKey"].ToString().Length > 0) writer.WriteString(r["ErrorKey"].ToString());
                    writer.WriteEndElement();
                    //--------------------------------------------------
                    writer.WriteStartElement("ErrorMessage");
                    if (r["ErrorMessage"].ToString().Length > 0) writer.WriteString(r["ErrorMessage"].ToString());
                    writer.WriteEndElement();
                    //--------------------------------------------------
                    writer.WriteEndElement(); //result
                    //--------------------------------------------------
                    writer.WriteEndElement(); //invoice status
                }
            }
            writer.WriteEndElement(); //transaction detail
            writer.WriteEndElement(); //batch interface report
            writer.Close();
        }

        public void ImportInvoice(string tbFileName, string conString, string dynamicsConString, string CompanyID, ImportApplication.Program.CompanySettings IPCompanySettings, string installDirectory, NameValueCollection AppSettings)
        {
            Console.WriteLine("Running....");

            int Success = 0;
            int Failure = 0;
            DataTable dt = new DataTable();
            DataTable et = new DataTable();
            DataTable dtdist = new DataTable();
            DataSet ds = new DataSet();
            string ErrorLog = "";
            string paidBatchNbr = "";
            string unpaidBatchNbr = "";
            string uniqueBatchNbr = "";

            // Setup GP Batch Numbers
            unpaidBatchNbr = AppSettings["GPBatchNbr_Unpaid"];
            if (string.IsNullOrEmpty(unpaidBatchNbr) == true)
            {
                unpaidBatchNbr = "IPY_UNPAID";
            }
                
            paidBatchNbr = AppSettings["GPBatchNbr_Paid"];
            if (string.IsNullOrEmpty(paidBatchNbr) == true)
            {
                paidBatchNbr = "IPY_PAID";
            }

            string poBatchNbr = AppSettings["POBatchNbr"];
            if (string.IsNullOrEmpty(poBatchNbr) == true)
            {
                poBatchNbr = "IPY_PO";
            }

            // Check for and add Unique components to the Batch Number if configured
            uniqueBatchNbr = AppSettings["UniqueGPBatchNbr"];
            if (string.IsNullOrEmpty(uniqueBatchNbr) != true)
                {
                   switch (uniqueBatchNbr.ToUpper()){
                       case "DAY":
                           unpaidBatchNbr = unpaidBatchNbr + "_" + DateTime.Now.ToString("yyyyMMdd");
                           paidBatchNbr = unpaidBatchNbr + "_" + DateTime.Now.ToString("yyyyMMdd");
                           poBatchNbr = poBatchNbr + "_" + DateTime.Now.ToString("yyyyMMdd");
                           break;
                       case "ALL":
                           unpaidBatchNbr = unpaidBatchNbr + "_" + DateTime.Now.ToString("yyyyMMddhhmm");
                           paidBatchNbr = unpaidBatchNbr + "_" + DateTime.Now.ToString("yyyyMMddhhmm");
                           poBatchNbr = poBatchNbr + "_" + DateTime.Now.ToString("yyyyMMddhhmm");
                           break;
                   }
                }

            //create list of columns for error table
            List<string> errorTableColumnNames = new List<string>() { "HdrID", "HdrText", "HdrErrorKey", "HdrErrorMsg",
                "TotalTransactions", "NewTransactions", "FailedTransactions", "DuplicateTransactions", "VendorNumber", 
                "InvoiceNumber", "InvoiceDate", "InvoiceTotal", "ID", "Text", "ErrorCount", "ErrorKey", "ErrorMessage", "LineNumber" };

            //add columns to errorTable
            foreach (string str in errorTableColumnNames)
            {
                et.Columns.Add(str);
            }

            eConnect_AP eConAP = new eConnect_AP(conString);
            eConnect_JE eConJE = new eConnect_JE(conString);
            eConnect_Receiving_Invoice eConPORcvInv = new eConnect_Receiving_Invoice(conString);

            // Load the XML file into memory
            ds.ReadXml(tbFileName);

            DataTable dtInvoices = ds.Tables["Invoice"];
            DataTable dtVendors = ds.Tables["Vendor"];
            DataTable dtBusinessUnits = ds.Tables["BusinessUnit"];
            DataTable dtAlternateIds = ds.Tables["AlternateId"];
            DataTable dtHeaderCustomFields = ds.Tables["HeaderCustomField"];
            DataTable dtLines = ds.Tables["Line"];
            DataTable dtDistributions = ds.Tables["Distribution"];
            DataTable dtDistributionCustomFields = ds.Tables["DistributionCustomField"];


            foreach (DataRow drInvoices in dtInvoices.Rows)
            {
                DataRow etr = et.NewRow(); //create new row for error table

                if (drInvoices["PurchaseOrderNbr"].ToString().Length != 0 && IPCompanySettings.UsesPurchaseOrders==1)  // PO Invoice
                {
                    // Load PO Invoice
                    eConPORcvInv.ResetVariables();

                    etr = CreatePOReceipt_InvoiceTransaction(poBatchNbr, drInvoices, dtVendors, dtLines, eConPORcvInv, CompanyID, dynamicsConString, AppSettings, etr);
                }
                else
                {
                    // Load Non-PO Invoices (Header with Distributions)
                    eConAP.ResetVariables();
                    eConJE.ResetVariables();
                    
                    if (drInvoices.IsNull("InvoiceNbr")) continue;

                    etr = CreateNonPOInvoiceTransaction(paidBatchNbr, unpaidBatchNbr, drInvoices, dtVendors, dtLines, dtBusinessUnits, dtAlternateIds, dtHeaderCustomFields, dtDistributions, dtDistributionCustomFields, eConAP, eConJE, CompanyID, IPCompanySettings, dynamicsConString, conString, etr, AppSettings);
                }

                if (etr["ErrorMessage"].ToString().Length > 0)
                {
                    et.Rows.Add(etr); //add new error row to error table for failed invoice
                    Failure += 1;
                }
                else
                {
                    Success += 1;
                }
               
            }

            string HeaderID = "";
            string HeaderText = "";
            string HeaderKey = "";
            string HeaderMsg = "";

            if (Failure == 0 && Success == 0)
            {
                ErrorLog = string.Format(@"No Imports Attempted. Successes: {0}; Failures: {1}" + Environment.NewLine + Environment.NewLine + ErrorLog, Success, Failure);
                HeaderID = "2";
                HeaderText = "Failed";
                HeaderMsg = "No Data Processed";
            }
            else if (Failure > 0 && Success == 0)
                    {
                ErrorLog = string.Format(@"No Imports Completed. Successes: {0}; Failures: {1}" + Environment.NewLine + Environment.NewLine + ErrorLog, Success, Failure);
                HeaderID = "2";
                HeaderText = "Failed";
                    }
            else if (Failure > 0 && Success > 0)
            {
                ErrorLog = string.Format(@"Completed With Errors. Successes: {0}; Failures: {1}" + Environment.NewLine + Environment.NewLine + ErrorLog, Success, Failure);
                HeaderID = "4";
                HeaderText = "Success with Errors";
                }
            else
            {
                ErrorLog = string.Format(@"Import Completed Successfully. Successes: {0}; Failures: {1}" + Environment.NewLine + Environment.NewLine + ErrorLog, Success, Failure);
                HeaderID = "3";
                HeaderText = "Successful";

                DataRow etr = et.NewRow();
                et.Rows.Add(etr); //add new error row to error table for failed invoice
            }

            //add data to error table
            for (int i = 0; i < et.Rows.Count; i++)
                {
                et.Rows[i]["HdrID"] = HeaderID;
                et.Rows[i]["HdrText"] = HeaderText;
                et.Rows[i]["HdrErrorKey"] = HeaderKey;
                et.Rows[i]["HdrErrorMsg"] = HeaderMsg;
                et.Rows[i]["HdrErrorMsg"] = "";//ErrorLog.ToString();
                et.Rows[i]["TotalTransactions"] = Convert.ToString(Success + Failure);
                et.Rows[i]["NewTransactions"] = Success.ToString();
                et.Rows[i]["FailedTransactions"] = Failure.ToString();
                et.Rows[i]["DuplicateTransactions"] = "0"; //create logic for this!
            }

            ErrorLog = System.DateTime.Now.ToString() + Environment.NewLine + ErrorLog;
            ExportErrorReport(installDirectory,tbFileName, et);

            System.IO.File.WriteAllText("ErrorLog.txt", ErrorLog);
        }

        private DataRow CreateNonPOInvoiceTransaction(string PaidBatchNbr, string UnpaidBatchNbr, DataRow Invoice, DataTable Vendors, DataTable Lines, DataTable BusinessUnits, DataTable AlternateIds, 
                                                      DataTable HeaderCustomFields, DataTable Distributions, DataTable DistributionCustomFields, eConnect_AP eConAP, 
                                                      eConnect_JE eConJE, string CompanyID, ImportApplication.Program.CompanySettings IPCompanySettings, string DynamicsConnectString, string CompanyDBConnectString, DataRow Response, NameValueCollection AppSettings)
            {
                int LineNumber = 0;

                 {
                    if (Invoice["TotalDue"].ToString() != "")
                    {
                        LineNumber++;

                        string InvoiceId = Invoice["Invoice_Id"].ToString();
                        string InvoiceNumber = Invoice["InvoiceNbr"].ToString();
                        string PaymentMethod = Invoice["PaymentMethod"].ToString();
                        string Status = Invoice["Status"].ToString();
                        List<DataRow> drVendors = Vendors.Select("[Invoice_Id] = '" + InvoiceId + "'").ToList();
                    string VendorNumber = drVendors[0]["CustomersVendorNbr"].ToString();
                    string APAccount = "";

                        if (CompanyID.Length > 0)
                        {
                        VendorNumber = VendorNumber.Substring(CompanyID.Length + 1, VendorNumber.Length - (CompanyID.Length + 1));
                    }

                    string VendorId = drVendors[0]["Vendor_Id"].ToString();
                    
                        List<DataRow> drBusinessUnits = BusinessUnits.Select("[Vendor_Id] = '" + VendorId + "'").ToList();
                    string BusinessUnitId = drBusinessUnits[0]["BusinessUnit_Id"].ToString();

                        if (AlternateIds != null)
                    {

                            List<DataRow> drAlternateIds = AlternateIds.Select("[BusinessUnit_Id] = '" + BusinessUnitId + "'"
                            + " AND [AlternateIdType] = 'AlternateID'").ToList();

                        if (drAlternateIds != null)
                        {
                            APAccount = drAlternateIds[0]["AlternateIDNbr"].ToString();
                        }
                    }

                    if (APAccount == "")
                    {
                        APAccount = IPCompanySettings.DefaultAPAccount;
                    }

					string PostingDate=""; 
                    if (IPCompanySettings.PostingDateOption == 1)
					{
						List<DataRow> drHeaderCustomFields = HeaderCustomFields.Select("[Invoice_Id] = '" + InvoiceId + "'"
						+ " AND [CFNbr] = '4'").ToList();
						PostingDate = drHeaderCustomFields[0]["Value"].ToString();
					}
                        string Description;

                        if (AppSettings["AltInvoiceDescription"].Length > 0) // Description has to have a Object Map of Header or Header Chartfield
                        {
                            Description = GetInvoiceFieldValueByObjectMap(AppSettings["AltInvoiceDescription"].ToString(), Invoice, HeaderCustomFields.Select("[Invoice_Id] = '" + InvoiceId + "'").ToList(),new DataTable().NewRow(), new List<DataRow>(), new DataTable().NewRow(), new List<DataRow>());
                        }
                        else
                        {
                            Description = Invoice["Source"].ToString();
                        }
                        
                        string DocDate = Invoice["InvoiceDate"].ToString();
                        string PaymentDate = Invoice["StatusDate"].ToString();
                        string PaymentReference = Invoice["PaymentReferenceNbr"].ToString();
                    string OffsetAccount = string.Empty;
                        decimal DocAmount = Convert.ToDecimal(Invoice["TotalDue"].ToString());
                    short DistType = 2;

                        Response["VendorNumber"] = VendorNumber;
                        Response["InvoiceNumber"] = InvoiceNumber;
                        Response["InvoiceDate"] = DocDate;
                        Response["InvoiceTotal"] = DocAmount.ToString();

					//Get any data configured for User Defined Fields
					string[] UserDefinedFieldValues = GetUserDefinedFields(Invoice, InvoiceId, HeaderCustomFields, AppSettings);

					// Get Currency Lookup Setting from App Settings
					string gpCurrencyID = "";
					if (AppSettings["LookupCurrency"] != null && AppSettings["LookupCurrency"].ToLower() == "true")
					{
						gpCurrencyID = LookupCurrency(Invoice["Currency"].ToString(), DynamicsConnectString);
					}

					Sql sql = new Sql();
					//get data from database
					//Sql sql = new Sql();
					//DataTable fromSql = sql.QueryDatabase(dynamicsConString, "SELECT TOP 1 * FROM DM_IPAYREF");

					//string DefaultCheckID = fromSql.Rows[0][2].ToString();
					//string Override = fromSql.Rows[0][3].ToString();
					string CashAccount = "";

                    try
                    {
                        DataTable fromSql;

                        if (PaymentMethod.Contains("AMEX"))
                        {
                                fromSql = sql.QueryDatabase(CompanyDBConnectString, "SELECT ACTNUMST FROM CM00100 c JOIN GL00105 gl ON c.ACTINDX = gl.ACTINDX WHERE c.CHEKBKID = 'AMEX'");
                        }
                        else
                        {
                                fromSql = sql.QueryDatabase(CompanyDBConnectString, "SELECT ACTNUMST FROM CM00100 c JOIN GL00105 gl ON c.ACTINDX = gl.ACTINDX WHERE c.CHEKBKID = '" + IPCompanySettings.CheckBook + "'");
                        }

                        CashAccount = fromSql.Rows[0][0].ToString();

                    }
                    catch (Exception ex)
                    {
                            Response["ErrorMessage"] += "Checkbook Does Not Exist!";
                            //DM_Helpers.InstallHelper.UpdateLogFile(installDirectory + @"\ConsoleApplicationErrorLog.txt", "Checkbook Does Not Exist!");
                    }

                    if (Status == "PAID")
                    {
                        if (IPCompanySettings.PostingDateOption == 1)
                        {
                                eConJE.SetTransactionHeader(PostingDate, PaidBatchNbr, VendorNumber + "-" + InvoiceNumber, 1, PaymentDate, UserDefinedFieldValues);
                        }
                        else
                        {
                                eConJE.SetTransactionHeader(DocDate, PaidBatchNbr, VendorNumber + "-" + InvoiceNumber, 1, PaymentDate, UserDefinedFieldValues);
                        }

                        DistType = 1;
                        OffsetAccount = CashAccount.Trim();

                            eConAP.InsertInvoice(VendorNumber, InvoiceNumber, DocDate, DocAmount, PaidBatchNbr, Description, PaymentDate, DocAmount, IPCompanySettings.CheckBook, PaymentDate, PaymentReference, gpCurrencyID, UserDefinedFieldValues);
                    }
                    else
                    {
                        OffsetAccount = APAccount.Trim();

                        if (IPCompanySettings.PostingDateOption == 1)
                        {
                            if (DocAmount > 0)
                            {
                                    eConAP.InsertInvoice(VendorNumber, InvoiceNumber, DocDate, DocAmount, UnpaidBatchNbr, Description, PostingDate, gpCurrencyID, UserDefinedFieldValues);
                            }
                            else
                            {
                                    eConAP.InsertCreditMemo(VendorNumber, InvoiceNumber, DocDate, Math.Abs(DocAmount), UnpaidBatchNbr, Description, PostingDate, gpCurrencyID, UserDefinedFieldValues);
                            }
                        }
                        else
                        {
                            if (DocAmount > 0)
                            {
                                    eConAP.InsertInvoice(VendorNumber, InvoiceNumber, DocDate, DocAmount, UnpaidBatchNbr, Description, DocDate, gpCurrencyID, UserDefinedFieldValues);
                            }
                            else
                            {
                                    eConAP.InsertCreditMemo(VendorNumber, InvoiceNumber, DocDate, Math.Abs(DocAmount), UnpaidBatchNbr, Description, DocDate, gpCurrencyID, UserDefinedFieldValues);
                            }
                        }
                    }
                    

                    List<DataRow> drDistributions = Distributions.Select("[Invoice_Id] = '" + InvoiceId + "'").ToList();

                    decimal totalOffsetAmount = 0;

                    //insert distributions
                    foreach (DataRow dist in drDistributions)
                    {
                        if (dist["Total"].ToString() != "")
                        {
                            decimal DistAmount = Convert.ToDecimal(dist["Total"].ToString());
                            string DistributionId = dist["Distribution_Id"].ToString();

                            List<DataRow> drDistributionCustomFields = DistributionCustomFields.Select("[Distribution_Id] = '" + DistributionId + "'" +
                            " AND [CFNbr] = '1'").ToList();

                            string DistributionAccount = drDistributionCustomFields[0]["Value"].ToString();
                            if (DistAmount > 0){
                                eConAP.InsertDistribution(DistributionAccount,6,DistAmount,0,"",VendorNumber);
                            }
                            else
                            {
                                eConAP.InsertDistribution(DistributionAccount,6,0,Math.Abs(DistAmount),"",VendorNumber);
                            }

                            if(Status == "PAID") 
                            {
                                eConJE.InsertDistribution(DistributionAccount, DistAmount, 0, "", InvoiceNumber, VendorNumber);
                            }

							if (AppSettings["AnalyticalAccountingField"] != null  && AppSettings["AnalyticalAccountingField"].Length > 0)
							{
								string aaFieldValue = GetInvoiceFieldValueByObjectMap(AppSettings["AnalyticalAccountingField"].ToString(), Invoice, HeaderCustomFields.Select("[Invoice_Id] = '" + InvoiceId + "'").ToList(), new DataTable().NewRow(), new List<DataRow>(), dist, DistributionCustomFields.Select("[Distribution_Id] = '" + DistributionId + "'").ToList());
								eConAP.InsertAADistribution(DistributionAccount, DistAmount, "", "",aaFieldValue,100,1);
							}

                            totalOffsetAmount += DistAmount;
                        }
                    }

                    //insert offset distribution
                    if (totalOffsetAmount > 0){
                        eConAP.InsertDistribution(OffsetAccount, DistType, 0, totalOffsetAmount, "", VendorNumber);
                        eConJE.InsertDistribution(APAccount, 0, totalOffsetAmount, "", InvoiceNumber, VendorNumber);
                    }
                    else
                    {
                        eConAP.InsertDistribution(OffsetAccount, DistType,  Math.Abs(totalOffsetAmount),0, "", VendorNumber);
                        eConJE.InsertDistribution(APAccount, Math.Abs(totalOffsetAmount),0, "", InvoiceNumber, VendorNumber);
                    }


                    eConAP.InsertTransaction();

                    if (eConAP.Failure > 0)
                    {
                        string errorString = eConAP.ErrorMessage.ToString();
                        string errorMsg = errorString.Substring(errorString.IndexOf("Error Description") + 20, errorString.IndexOf("Node Identifier") - errorString.IndexOf("Error Description") - 20);
                        string errorKey = errorString.Substring(errorString.IndexOf("Error Number") + 15, errorString.IndexOf("Stored Procedure") - errorString.IndexOf("Error Number") - 15);

                        Response["ID"] = 2;
                        Response["Text"] = "Failed";
                        Response["ErrorMessage"] = errorMsg;
                        Response["ErrorKey"] = errorKey;
                        Response["LineNumber"] = LineNumber;

                    }
                    else
                    {
                        eConJE.InsertTransaction();
                    }

                }

            }
                 return Response;
            }

        private string GetInvoiceFieldValueByObjectMap(string FieldMap, DataRow Invoice, List<DataRow> HeaderCustomFields, DataRow Line, List<DataRow> LineCustomFields, DataRow Distribution, List<DataRow> DistributionCustomFields)
        {
            string[] stringSeparators = new string[] {"."};
            string[] mapParts = FieldMap.Split(stringSeparators, StringSplitOptions.None);
            DataRow chartField;

            if (mapParts.Count() > 2)
                // Not a Header level field - chartfield or distribution

            {
                switch (mapParts[1].ToString())
                {
                    case "Chartfields":
                        for(int i=0;i<HeaderCustomFields.Count;i++){
                            chartField = HeaderCustomFields[i];
                            if (chartField["CFNbr"].ToString() == mapParts[2].ToString())
                            {
                                return chartField["Value"].ToString();
                            }
                        }
                        break;
					case "Line":
						// Handle in the Future
						break;
					case "Distribution":
						if (mapParts.Count() > 3) // distribution Chartfield
						{
							for (int i = 0; i < DistributionCustomFields.Count; i++)
							{
								chartField = DistributionCustomFields[i];
								if (chartField["CFNbr"].ToString() == mapParts[3].ToString())
								{
									return chartField["Value"].ToString();
								}
							}
						}
						else
						{
							return Invoice[mapParts[2]].ToString();
						}
						break;
                    default:
                        return "";
                }
            }
            else
            {
                return Invoice[mapParts[1]].ToString();
            }
            return "";
        }

        private DataRow CreatePOReceipt_InvoiceTransaction(string BatchNbr, DataRow Invoice, DataTable Vendors, DataTable Lines, eConnect_Receiving_Invoice eConPORcvInv, string CompanyID, string DynamicsConnectString, NameValueCollection AppSettings, DataRow Response)
            {
            // Create Invoice Header Entry
            // Get the internal Data Table ID for the Invoice
            string InvoiceId = Invoice["Invoice_Id"].ToString();

            List<DataRow> vendor = Vendors.Select("[Invoice_Id] = '" + InvoiceId + "'").ToList();

            string custVendorNbr = vendor[0]["CustomersVendorNbr"].ToString();
            if (CompanyID.Length > 0)
            {
                custVendorNbr = custVendorNbr.Substring(CompanyID.Length + 1, custVendorNbr.Length - (CompanyID.Length + 1));
            }

            string vendorName = vendor[0]["Name"].ToString();
            string invoiceNbr = Invoice["InvoiceNbr"].ToString();
            string invoiceDate = Invoice["InvoiceDate"].ToString();
            Decimal lineSum = Convert.ToDecimal(Invoice["LineSum"].ToString());
            Decimal invoiceFreight = Convert.ToDecimal(Invoice["Freight"].ToString());
            Decimal invoiceTax = Convert.ToDecimal(Invoice["TaxAmount"].ToString());
            string invoiceSource = Invoice["Source"].ToString();
            string postingDate = DateTime.Now.ToString();
            string poNbr = Invoice["PurchaseOrderNbr"].ToString();

			// Lookup the GP Currency code based on IW ISO Currency Code **If Configured for Currency Lookup in App.config
			string gpCurrencycode = "";
			if (AppSettings["LookupCurrency"] != null && AppSettings["LookupCurrency"].ToLower() == "true")
			{
				gpCurrencycode = LookupCurrency(Invoice["Currency"].ToString(), DynamicsConnectString);
			}

            // Store the key invoice data in the response
            Response["VendorNumber"] = custVendorNbr;
            Response["InvoiceNumber"] = invoiceNbr;
            Response["InvoiceDate"] = invoiceDate;
            Response["InvoiceTotal"] = (lineSum + invoiceFreight + invoiceTax).ToString();

            eConPORcvInv.InsertInvoice(custVendorNbr, vendorName, invoiceNbr, invoiceDate, lineSum, invoiceFreight, invoiceTax, BatchNbr, invoiceSource, postingDate, gpCurrencycode);

            // Create Invoice Line Entries
            // Get the Lines that belong to this invoice
            List<DataRow> invoiceLines = Lines.Select("[Invoice_Id] = '" + InvoiceId + "'").ToList();

            foreach (DataRow line in invoiceLines)
            {
                Decimal quantity = Convert.ToDecimal(line["Quantity"].ToString());
                string itemID = line["ItemID_SKU"].ToString();
                string description = line["Description"].ToString();
                string unitOfMeasure = line["UnitOfMeasure"].ToString();
                Decimal unitCost = Convert.ToDecimal(line["UnitCost"].ToString());
                Decimal lineTotal = Convert.ToDecimal(line["LineTotal"].ToString());
                Int32 poLineNbr = Convert.ToInt32(line["POLineNbr"].ToString());
                eConPORcvInv.InsertInvoiceLine(poNbr, quantity, itemID, description, custVendorNbr, unitOfMeasure, unitCost, lineTotal, poLineNbr, DynamicsConnectString, CompanyID);
            }

            eConPORcvInv.InsertTransaction();

            if (eConPORcvInv.Failure > 0)
            {
                string errorString = eConPORcvInv.ErrorMessage.ToString();
                string errorMsg = errorString.Substring(errorString.IndexOf("Error Description") + 20, errorString.IndexOf("Node Identifier") - errorString.IndexOf("Error Description") - 20);
                string errorKey = errorString.Substring(errorString.IndexOf("Error Number") + 15, errorString.IndexOf("Stored Procedure") - errorString.IndexOf("Error Number") - 15);

                Response["ID"] = 2;
                Response["Text"] = "Failed";
                Response["ErrorMessage"] = errorMsg;
                Response["ErrorKey"] = errorKey;
            }

            return Response;

            //ErrorLog += eConAP.ErrorMessage;
            //Success += eConAP.Success;
            //Failure += eConAP.Failure;

            //return errorRow;
        }

		private string LookupCurrency(string IPCurrencyCode, string dynamicsConString)
		{
			string gpCurrencyID = "";

			// Lookup the Currency from the Lookup Table in the Dynamics DB
			Sql SQL = new Sql();
			DataTable fromSql = SQL.QueryDatabase(dynamicsConString, "SELECT TOP 1 * FROM CurrencyXRef WHERE ISOThree = '" + IPCurrencyCode + "'");
			if (fromSql.Rows.Count > 0) { 
				gpCurrencyID = fromSql.Rows[0][1].ToString();
			}
			return gpCurrencyID;
		}

		private string Left(string inString, int inInt){
        
            if (inInt > inString.Length)
                inInt = inString.Length;
            return inString.Substring(0, inInt);
        }

        private string Right(string inString, int inInt){

            if (inInt > inString.Length)
            {
                return inString;
            }
            else
            {
                return inString.Substring(inString.Length - inInt, inInt);
            }
        }

		private string[] GetUserDefinedFields(DataRow Invoice , string InvoiceId, DataTable HeaderCustomFields, NameValueCollection AppSettings)
		{
			string[] fieldValues = new string[6];

			int idx;

			for (idx = 1; idx < 6; idx++)
			{
				if (AppSettings["HeaderUDF" + idx.ToString()] != null)
                {
					fieldValues[idx] = GetInvoiceFieldValueByObjectMap(AppSettings["HeaderUDF" + idx.ToString()], Invoice, HeaderCustomFields.Select("[Invoice_Id] = '" + InvoiceId + "'").ToList(), new DataTable().NewRow(),new List<DataRow>(), new DataTable().NewRow(), new List<DataRow>());
				}
			}

			return fieldValues;
		}
    }
}
