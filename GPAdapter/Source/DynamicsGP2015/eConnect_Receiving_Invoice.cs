﻿using System;
using System.Data;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Dynamics.GP.eConnect;
using Microsoft.Dynamics.GP.eConnect.Serialization;
//using Microsoft.Dynamics.GP.eConnect.MiscRoutines;
using DM_Helpers;

namespace ImportApplication
{
    class eConnect_Receiving_Invoice
    {
        private string connectionString;
        private taPopRcptHdrInsert transaction;
        private List<taPopRcptLineInsert_ItemsTaPopRcptLineInsert> lines;
        private List<taAnalyticsDistribution_ItemsTaAnalyticsDistribution> aaDistributions;
        private int lineSequence;
        private string ReceiptNbr;
        public int Success { get; set; }
        public int Failure { get; set; }

        public string ErrorMessage { get; set; }

        public eConnect_Receiving_Invoice(string connString)
        {
            connectionString = connString;
            transaction = new taPopRcptHdrInsert();
            lines = new List<taPopRcptLineInsert_ItemsTaPopRcptLineInsert>();

            
            aaDistributions = new List<taAnalyticsDistribution_ItemsTaAnalyticsDistribution>();
            Success = 0;
            Failure = 0;
            ErrorMessage = "";
            GetNextDocNumbers doc = new GetNextDocNumbers();
            ReceiptNbr = doc.GetNextPOPReceiptNumber(IncrementDecrement.Increment, connectionString);
            //VoucherNumber = doc.GetPMNextVoucherNumber(Microsoft.Dynamics.GP.eConnect.MiscRoutines.GetNextDocNumbers.IncrementDecrement.Increment, connectionString);
        }

        public void ResetVariables()
        {
            transaction = new taPopRcptHdrInsert();
            lines = new List<taPopRcptLineInsert_ItemsTaPopRcptLineInsert>();
            //distributions = new List<taPMDistribution_ItemsTaPMDistribution>();
            //aaDistributions = new List<taAnalyticsDistribution_ItemsTaAnalyticsDistribution>();
            Success = 0;
            Failure = 0;
            ErrorMessage = "";
            GetNextDocNumbers doc = new GetNextDocNumbers();
            ReceiptNbr = doc.GetNextPOPReceiptNumber(IncrementDecrement.Increment, connectionString);
            //VoucherNumber = doc.GetPMNextVoucherNumber(Microsoft.Dynamics.GP.eConnect.MiscRoutines.GetNextDocNumbers.IncrementDecrement.Increment, connectionString);
        }

        public void InsertInvoice(string VendorId,
                                    string VendorName,
                                    string VendorDocumentNumber,
                                    string Date,
                                    Decimal LineSum,
                                    Decimal InvoiceFreight,
                                    Decimal InvoiceTax,
                                    string Batch,
                                    string Description,
                                    string PostingDate,
									string GPCurrencyID)
        {
            transaction.POPRCTNM = ReceiptNbr;
            transaction.POPTYPE = 3; // 1 = Shipment, 3=Shipment/invoice
            //transaction.DOCTYPE = 1;
            transaction.VNDDOCNM = VendorDocumentNumber;
            transaction.receiptdate = Date;
            transaction.BACHNUMB = Batch;
            transaction.VENDORID = VendorId;
            transaction.VENDNAME = VendorName;
            transaction.SUBTOTAL = LineSum;
            transaction.FRTAMNT = InvoiceFreight;
            transaction.TAXAMNT = InvoiceTax;
            //transaction.AUTOCOST = 0;
            transaction.CREATEDIST = 1;
            //transaction.TRXDSCRN = Description;
            //transaction.PSTGDATE = PostingDate;
            lineSequence = 0;
			if (GPCurrencyID.Length > 0)
			{
				transaction.CURNCYID = GPCurrencyID;
			}
        }

        public void InsertInvoiceLine(string PONbr,
                                      Decimal Quantity,
                                      string ItemID_SKU,
                                      string Description,
                                      string VendorID,
                                      string UnitOfMeasure,
                                      Decimal UnitCost,
                                      Decimal LineTotal,
                                      Int32 POLineNbr,
                                      string DynamicsConnectString,
                                      string CompanyID)
        {
            // Lookup Line to get some information not passed through InvoiceWorks
            DataTable poLine = GPPOLine(CompanyID, PONbr, POLineNbr, DynamicsConnectString);
            
            taPopRcptLineInsert_ItemsTaPopRcptLineInsert line = new taPopRcptLineInsert_ItemsTaPopRcptLineInsert();
            line.POPTYPE = 3; //1=Shipment, 3=Shipment/invoice
            line.POPRCTNM = ReceiptNbr;
            line.PONUMBER = PONbr;
            line.QTYSHPPD = Quantity;
            line.QTYINVCD = Quantity;
            //line.POLNENUM = POLineNbr;
            line.ITEMNMBR = ItemID_SKU;
            line.VNDITNUM = poLine.Rows[0]["VNDITNUM"].ToString().TrimEnd();
            //line.VNDITNUM = ItemID_SKU;
            line.VNDITDSC = Description;
            line.VENDORID = VendorID;
            line.UNITCOSTSpecified = true;
            line.UOFM = UnitOfMeasure;
            line.UNITCOST = UnitCost;
            //line.EXTDCOST = LineTotal;
            //line.AUTOCOST = 0;
            //line.POLNENUM = POLineNbr;
            line.NONINVEN = Convert.ToInt16(poLine.Rows[0]["NONINVEN"]);
            line.INVINDX = Convert.ToInt32(poLine.Rows[0]["INVINDX"]);
            line.POLNENUM = Convert.ToInt32(poLine.Rows[0]["ORD"]);
            lines.Add(line);

        }
        //public void InsertCreditMemo(string VendorId,
        //                    string VendorDocumentNumber,
        //                    string Date,
        //                    Decimal Amount,
        //                    string Batch,
        //                    string Description,
        //                    string PostingDate)
        //{
        //    transaction.VCHNUMWK = VoucherNumber;
        //    transaction.DOCTYPE = 5;
        //    transaction.DOCNUMBR = VendorDocumentNumber;
        //    transaction.DOCDATE = Date;
        //    transaction.DOCAMNT = Amount;
        //    transaction.BACHNUMB = Batch;
        //    transaction.VENDORID = VendorId;
        //    transaction.PRCHAMNT = Amount;
        //    transaction.CHRGAMNT = Amount;
        //    transaction.CREATEDIST = 0;
        //    transaction.TRXDSCRN = Description;
        //    transaction.PSTGDATE = PostingDate;
        //    lineSequence = 0;
        //}

        //public void InsertInvoice(string VendorId,
        //                            string VendorDocumentNumber,
        //                            string Date,
        //                            Decimal Amount,
        //                            string Batch,
        //                            string Description,
        //                            string PostingDate,
        //                            Decimal PaymentAmount,
        //                            string CheckbookID,
        //                            string PaymentDate,
        //                            string PaymentNumber)
        //{
        //    transaction.VCHNUMWK = VoucherNumber;
        //    transaction.DOCTYPE = 1;
        //    transaction.DOCNUMBR = VendorDocumentNumber;
        //    transaction.DOCDATE = Date;
        //    transaction.DOCAMNT = Amount;
        //    transaction.BACHNUMB = Batch;
        //    transaction.VENDORID = VendorId;
        //    transaction.PRCHAMNT = Amount;
        //    transaction.CHRGAMNT = 0;
        //    transaction.CREATEDIST = 0;
        //    transaction.TRXDSCRN = Description;
        //    transaction.CHEKAMNT = PaymentAmount;
        //    transaction.CHAMCBID = CheckbookID;
        //    transaction.CHEKDATE = PaymentDate;
        //    transaction.CAMPYNBR = PaymentNumber;
        //    transaction.CHEKNMBR = PaymentNumber;
        //    transaction.PSTGDATE = PostingDate;
        //    lineSequence = 0;
        //}

        //public void InsertDistribution(string AccountNumber,
        //                                short DistributionType,
        //                                Decimal DebitAmount,
        //                                Decimal CreditAmount,
        //                                string Reference,
        //                                string Vendor)
        //{
        //    taPMDistribution_ItemsTaPMDistribution dist = new taPMDistribution_ItemsTaPMDistribution();
        //    lineSequence += 16384;
        //    dist.VCHRNMBR = VoucherNumber;
        //    dist.ACTNUMST = AccountNumber;
        //    dist.DISTTYPE = DistributionType;
        //    dist.DEBITAMT = DebitAmount;
        //    dist.CRDTAMNT = CreditAmount;
        //    dist.DistRef = Reference;
        //    dist.VENDORID = Vendor;
        //    dist.DSTSQNUM = lineSequence;
        //    dist.DOCTYPE = 1;
        //    distributions.Add(dist);
        //}

        //public void InsertAADistribution(string AccountNumber, Decimal Amount, string DistRef, string Dimension, string Code, Decimal Percentage, int aaLine)
        //{
        //    taAnalyticsDistribution_ItemsTaAnalyticsDistribution aaItem = new taAnalyticsDistribution_ItemsTaAnalyticsDistribution();
        //    aaItem.aaAssignedPercent = Percentage;
        //    if (Percentage != 100) { aaItem.aaAssignedPercentSpecified = true; }
        //    aaItem.ACTNUMST = AccountNumber;
        //    aaItem.AMOUNTSpecified = true;
        //    aaItem.AMOUNT = Amount;
        //    aaItem.DistRef = DistRef;
        //    aaItem.DistSequenceSpecified = true;
        //    aaItem.DistSequence = lineSequence;
        //    aaItem.aaTrxDim = Dimension;
        //    aaItem.aaTrxDimCode = Code;
        //    aaItem.DOCNMBR = VoucherNumber;
        //    aaItem.aaSubLedgerAssignID = aaLine;
        //    aaDistributions.Add(aaItem);
        //}


        public void InsertTransaction()
        {
            eConnectType eConnect = new eConnectType();
            POPReceivingsType[] pmArray = new POPReceivingsType[1];
            POPReceivingsType pmType = new POPReceivingsType();
            pmType.taPopRcptHdrInsert = transaction;
            pmType.taPopRcptLineInsert_Items = lines.ToArray();
            pmArray[0] = pmType;
            eConnect.POPReceivingsType = pmArray;

            if (XMLSerialize("PORcvInv.xml", eConnect))
            {
                if (eConnectEntry("PORcvInv.xml"))
                {
                    Success++;
                    //MessageBox.Show("Success");
                }
                else
                {
                    Failure++;
                    //ErrorLogging.WriteErrorLines(dt);
                }
            }

        }

        private bool eConnectEntry(string filename)
        {
            //return true;

            bool message = false;

            eConnectMethods eConnect = new eConnectMethods();
            XmlDocument myXmlDoc = new XmlDocument();
            XmlNode eConnectProcessInfoOutgoing;
            try
            {
                myXmlDoc.Load(filename);
                eConnectProcessInfoOutgoing = myXmlDoc.SelectSingleNode("//Outgoing");
                if ((eConnectProcessInfoOutgoing == null) || (string.IsNullOrEmpty(eConnectProcessInfoOutgoing.InnerText) == true))
                {
                    eConnect.CreateTransactionEntity(connectionString, myXmlDoc.OuterXml);
                    //eConnect.eConnect_EntryPoint(connectionString, EnumTypes.ConnectionStringType.SqlClient, myXmlDoc.OuterXml, EnumTypes.SchemaValidationType.None, "");
                    message = true;
                }
                //else
                //{
                //    if (eConnectProcessInfoOutgoing.InnerText == "TRUE")
                //    {
                //        string error = eConnect.
                //        string error = eConnect.GetEntity(connectionString, myXmlDoc.OuterXml);
                //        //ErrorLogging.WriteToFile("Error" + error);
                //        //ErrorLog += error + "\n";
                //        ErrorMessage = error;
                //        message = false;
                //    }
                //}
            }
            catch (Exception ex)
            {
                message = false;
                //ErrorLog += ex.Message.ToString() + "\n";
                ErrorMessage = ex.Message.ToString() + Environment.NewLine;
                if (ex.Message == "An item with the same key has already been added.")
                {
                    ErrorMessage += "<VENDORID>" + transaction.VENDORID + "</VENDORID>" + Environment.NewLine +
                        "<INVOICENUMBER>" + transaction.VNDDOCNM + "</INVOICENUMBER>" + Environment.NewLine;
                    foreach (taAnalyticsDistribution_ItemsTaAnalyticsDistribution aa in aaDistributions)
                    {
                        ErrorMessage += "<" + aa.aaTrxDim + ">" + aa.aaTrxDimCode + "</" + aa.aaTrxDim + ">" + Environment.NewLine;
                    }
                }
            }
            finally
            {
                eConnect.Dispose();
            }
            return message;

        }

        private bool XMLSerialize(string filename, eConnectType eConnect)
        {
            try
            {
                // Create a file to hold the serialized eConnect XML document
                FileStream fs = new FileStream(filename, FileMode.Create);
                XmlTextWriter writer = new XmlTextWriter(fs, new UTF8Encoding());

                // Serialize the eConnect document object to the file using the XmlTextWriter.
                XmlSerializer serializer = new XmlSerializer(eConnect.GetType());
                serializer.Serialize(writer, eConnect);
                writer.Close();
                return true;
            }
            catch (Exception ex)
            {
                ErrorMessage += ex.Message + Environment.NewLine;
                return false;
            }
        }

        private DataTable GPPOLine(string GPCompanyID, string PONbr, int LineNbr, string DynamicsConnectString)
        {
            Sql sql = new Sql();

            DataTable lines = sql.QueryDatabase(DynamicsConnectString, "EXEC usp_iPayables_POLine_Lookup '" + GPCompanyID + "','" + PONbr + "','" + LineNbr + "'" );

            if (lines.Rows.Count == 1)
            {
                return lines;
            }
            else
            {
                return null;
            }
        }
    }
}

