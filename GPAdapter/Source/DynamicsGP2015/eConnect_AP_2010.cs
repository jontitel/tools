﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Dynamics.GP.eConnect;
using Microsoft.Dynamics.GP.eConnect.Serialization;
//using Microsoft.Dynamics.GP.eConnect.MiscRoutines;

namespace ImportApplication
{
    class eConnect_AP
    {
        private string connectionString;
        private taPMTransactionInsert transaction;
        private List<taPMDistribution_ItemsTaPMDistribution> distributions;
        private List<taAnalyticsDistribution_ItemsTaAnalyticsDistribution> aaDistributions;
        private int lineSequence;
        private string VoucherNumber;
        public int Success { get; set; }
        public int Failure { get; set; }

        public string ErrorMessage { get; set; }

        public eConnect_AP(string connString)
        {
            connectionString = connString;
            transaction = new taPMTransactionInsert();
            distributions = new List<taPMDistribution_ItemsTaPMDistribution>();
            aaDistributions = new List<taAnalyticsDistribution_ItemsTaAnalyticsDistribution>();
            Success = 0;
            Failure = 0;
            ErrorMessage = "";
            GetNextDocNumbers doc = new GetNextDocNumbers();
            VoucherNumber = doc.GetPMNextVoucherNumber(IncrementDecrement.Increment, connectionString);
            //VoucherNumber = doc.GetPMNextVoucherNumber(Microsoft.Dynamics.GP.eConnect.MiscRoutines.GetNextDocNumbers.IncrementDecrement.Increment, connectionString);
        }

        public void ResetVariables()
        {
            transaction = new taPMTransactionInsert();
            distributions = new List<taPMDistribution_ItemsTaPMDistribution>();
            aaDistributions = new List<taAnalyticsDistribution_ItemsTaAnalyticsDistribution>();
            Success = 0;
            Failure = 0;
            ErrorMessage = "";
            GetNextDocNumbers doc = new GetNextDocNumbers();
            VoucherNumber = doc.GetPMNextVoucherNumber(IncrementDecrement.Increment, connectionString);
            //VoucherNumber = doc.GetPMNextVoucherNumber(Microsoft.Dynamics.GP.eConnect.MiscRoutines.GetNextDocNumbers.IncrementDecrement.Increment, connectionString);
        }

        public void InsertInvoice(string VendorId,
                                    string VendorDocumentNumber,
                                    string Date,
                                    Decimal Amount,
                                    string Batch,
                                    string Description,
                                    string PostingDate,
									string GPCurrencyID, 
									string[] UserDefinedFieldValues)
        {
            transaction.VCHNUMWK = VoucherNumber;
            transaction.DOCTYPE = 1;
            transaction.DOCNUMBR = VendorDocumentNumber;
            transaction.DOCDATE = Date;
            transaction.DOCAMNT = Amount;
            transaction.BACHNUMB = Batch;
            transaction.VENDORID = VendorId;
            transaction.PRCHAMNT = Amount;
            transaction.CHRGAMNT = Amount;
            transaction.CREATEDIST = 0;
            transaction.TRXDSCRN = Description;
            transaction.PSTGDATE = PostingDate;
			if (GPCurrencyID.Length > 0)
			{
				transaction.CURNCYID = GPCurrencyID;
			}
            lineSequence = 0;

			// Insert any values configured for User Defined Fields
			if (UserDefinedFieldValues[1] != null) transaction.USRDEFND1 = UserDefinedFieldValues[1];
			if (UserDefinedFieldValues[2] != null) transaction.USRDEFND2 = UserDefinedFieldValues[2];
			if (UserDefinedFieldValues[3] != null) transaction.USRDEFND3 = UserDefinedFieldValues[3];
			if (UserDefinedFieldValues[4] != null) transaction.USRDEFND4 = UserDefinedFieldValues[4];
			if (UserDefinedFieldValues[5] != null) transaction.USRDEFND5 = UserDefinedFieldValues[5];
        }

        public void InsertCreditMemo(string VendorId,
                            string VendorDocumentNumber,
                            string Date,
                            Decimal Amount,
                            string Batch,
                            string Description,
                            string PostingDate,
							string GPCurrencyID,
							string[] UserDefinedFieldValues)
        {
            transaction.VCHNUMWK = VoucherNumber;
            transaction.DOCTYPE = 5;
            transaction.DOCNUMBR = VendorDocumentNumber;
            transaction.DOCDATE = Date;
            transaction.DOCAMNT = Amount;
            transaction.BACHNUMB = Batch;
            transaction.VENDORID = VendorId;
            transaction.PRCHAMNT = Amount;
            transaction.CHRGAMNT = Amount;
            transaction.CREATEDIST = 0;
            transaction.TRXDSCRN = Description;
            transaction.PSTGDATE = PostingDate;
			if (GPCurrencyID.Length > 0)
			{
				transaction.CURNCYID = GPCurrencyID;
			}
			lineSequence = 0;

			// Insert any values configured for User Defined Fields
			if (UserDefinedFieldValues[1] != null) transaction.USRDEFND1 = UserDefinedFieldValues[1];
			if (UserDefinedFieldValues[2] != null) transaction.USRDEFND2 = UserDefinedFieldValues[2];
			if (UserDefinedFieldValues[3] != null) transaction.USRDEFND3 = UserDefinedFieldValues[3];
			if (UserDefinedFieldValues[4] != null) transaction.USRDEFND4 = UserDefinedFieldValues[4];
			if (UserDefinedFieldValues[5] != null) transaction.USRDEFND5 = UserDefinedFieldValues[5];
        }

        public void InsertInvoice(string VendorId,
                                    string VendorDocumentNumber,
                                    string Date,
                                    Decimal Amount,
                                    string Batch,
                                    string Description,
                                    string PostingDate,
                                    Decimal PaymentAmount,
                                    string CheckbookID,
                                    string PaymentDate,
                                    string PaymentNumber,
									string GPCurrencyID,
									string[] UserDefinedFieldValues)
        {
            transaction.VCHNUMWK = VoucherNumber;
            transaction.DOCTYPE = 1;
            transaction.DOCNUMBR = VendorDocumentNumber;
            transaction.DOCDATE = Date;
            transaction.DOCAMNT = Amount;
            transaction.BACHNUMB = Batch;
            transaction.VENDORID = VendorId;
            transaction.PRCHAMNT = Amount;
            transaction.CHRGAMNT = 0;
            transaction.CREATEDIST = 0;
            transaction.TRXDSCRN = Description;
            transaction.CHEKAMNT = PaymentAmount;
            transaction.CHAMCBID = CheckbookID;
            transaction.CHEKDATE = PaymentDate;
            transaction.CAMPYNBR = PaymentNumber;
            transaction.CHEKNMBR = PaymentNumber;
            transaction.PSTGDATE = PostingDate;

			// Include Currency if configured for Currency ID lookup and lookup returned a value;
			if (GPCurrencyID.Length > 0)
			{
				transaction.CURNCYID = GPCurrencyID;
			}

			// Insert any values configured for User Defined Fields
			if (UserDefinedFieldValues[1] != null) transaction.USRDEFND1 = UserDefinedFieldValues[1];
			if (UserDefinedFieldValues[2] != null) transaction.USRDEFND2 = UserDefinedFieldValues[2];
			if (UserDefinedFieldValues[3] != null) transaction.USRDEFND3 = UserDefinedFieldValues[3];
			if (UserDefinedFieldValues[4] != null) transaction.USRDEFND4 = UserDefinedFieldValues[4];
			if (UserDefinedFieldValues[5] != null) transaction.USRDEFND5 = UserDefinedFieldValues[5];

            lineSequence = 0;

        }

        public void InsertDistribution(string AccountNumber,
                                        short DistributionType,
                                        Decimal DebitAmount,
                                        Decimal CreditAmount,
                                        string Reference,
                                        string Vendor)
        {
            taPMDistribution_ItemsTaPMDistribution dist = new taPMDistribution_ItemsTaPMDistribution();
            lineSequence += 16384;
            dist.VCHRNMBR = VoucherNumber;
            dist.ACTNUMST = AccountNumber;
            dist.DISTTYPE = DistributionType;
            dist.DEBITAMT = DebitAmount;
            dist.CRDTAMNT = CreditAmount;
            dist.DistRef = Reference;
            dist.VENDORID = Vendor;
            dist.DSTSQNUM = lineSequence;
            dist.DOCTYPE = 1;
            distributions.Add(dist);
        }

        public void InsertAADistribution(string AccountNumber, Decimal Amount, string DistRef, string Dimension, string Code, Decimal Percentage, int aaLine)
        {
            taAnalyticsDistribution_ItemsTaAnalyticsDistribution aaItem = new taAnalyticsDistribution_ItemsTaAnalyticsDistribution();
            aaItem.aaAssignedPercent = Percentage;
            if (Percentage != 100) { aaItem.aaAssignedPercentSpecified = true; }
            aaItem.ACTNUMST = AccountNumber;
            aaItem.AMOUNTSpecified = true;
            aaItem.AMOUNT = Amount;
            aaItem.DistRef = DistRef;
            aaItem.DistSequenceSpecified = true;
            aaItem.DistSequence = lineSequence;
            aaItem.aaTrxDim = Dimension;
            aaItem.aaTrxDimCode = Code;
            aaItem.DOCNMBR = VoucherNumber;
            aaItem.aaSubLedgerAssignID = aaLine;
            aaDistributions.Add(aaItem);
        }


        public void InsertTransaction()
        {
            eConnectType eConnect = new eConnectType();
            PMTransactionType[] pmArray = new PMTransactionType[1];
            PMTransactionType pmType = new PMTransactionType();
            pmType.taPMTransactionInsert = transaction;
            pmType.taPMDistribution_Items = distributions.ToArray();
            pmType.taAnalyticsDistribution_Items = aaDistributions.ToArray();
            pmArray[0] = pmType;
            eConnect.PMTransactionType = pmArray;

            if (XMLSerialize("AP.xml", eConnect))
            {
                if (eConnectEntry("AP.xml"))
                {
                    Success++;
                    //MessageBox.Show("Success");
                }
                else
                {
                    Failure++;
                    //ErrorLogging.WriteErrorLines(dt);
                }
            }

        }

        private bool eConnectEntry(string filename)
        {
            //return true;

            bool message = false;

            eConnectMethods eConnect = new eConnectMethods();
            XmlDocument myXmlDoc = new XmlDocument();
            XmlNode eConnectProcessInfoOutgoing;
            try
            {
                myXmlDoc.Load(filename);
                eConnectProcessInfoOutgoing = myXmlDoc.SelectSingleNode("//Outgoing");
                if ((eConnectProcessInfoOutgoing == null) || (string.IsNullOrEmpty(eConnectProcessInfoOutgoing.InnerText) == true))
                {
                    eConnect.CreateTransactionEntity(connectionString, myXmlDoc.OuterXml);
                    //eConnect.eConnect_EntryPoint(connectionString, EnumTypes.ConnectionStringType.SqlClient, myXmlDoc.OuterXml, EnumTypes.SchemaValidationType.None, "");
                    message = true;
                }
                //else
                //{
                //    if (eConnectProcessInfoOutgoing.InnerText == "TRUE")
                //    {
                //        string error = eConnect.
                //        string error = eConnect.GetEntity(connectionString, myXmlDoc.OuterXml);
                //        //ErrorLogging.WriteToFile("Error" + error);
                //        //ErrorLog += error + "\n";
                //        ErrorMessage = error;
                //        message = false;
                //    }
                //}
            }
            catch (Exception ex)
            {
                message = false;
                //ErrorLog += ex.Message.ToString() + "\n";
                ErrorMessage = ex.Message.ToString() + Environment.NewLine;
                if (ex.Message == "An item with the same key has already been added.")
                {
                    ErrorMessage += "<VENDORID>" + transaction.VENDORID + "</VENDORID>" + Environment.NewLine +
                        "<INVOICENUMBER>" + transaction.DOCNUMBR + "</INVOICENUMBER>" + Environment.NewLine;
                    foreach (taAnalyticsDistribution_ItemsTaAnalyticsDistribution aa in aaDistributions)
                    {
                        ErrorMessage += "<" + aa.aaTrxDim + ">" + aa.aaTrxDimCode + "</" + aa.aaTrxDim + ">" + Environment.NewLine;
                    }
                }
            }
            finally
            {
                eConnect.Dispose();
            }
            return message;

        }

        private bool XMLSerialize(string filename, eConnectType eConnect)
        {
            try
            {
                // Create a file to hold the serialized eConnect XML document
                FileStream fs = new FileStream(filename, FileMode.Create);
                XmlTextWriter writer = new XmlTextWriter(fs, new UTF8Encoding());

                // Serialize the eConnect document object to the file using the XmlTextWriter.
                XmlSerializer serializer = new XmlSerializer(eConnect.GetType());
                serializer.Serialize(writer, eConnect);
                writer.Close();
                return true;
            }
            catch (Exception ex)
            {
                ErrorMessage += ex.Message + Environment.NewLine;
                return false;
            }
        }
    }
}

