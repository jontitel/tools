﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class ApplicationHelpers
    {

        public static string DecryptString(string EncryptedText, string First, string Second)
        {
            byte[] encryptedTextBytes = Convert.FromBase64String(EncryptedText);

            MemoryStream ms = new MemoryStream();

            System.Security.Cryptography.SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();


            byte[] rgbIV = Encoding.ASCII.GetBytes(First);
            byte[] key = Encoding.ASCII.GetBytes(Second);

            CryptoStream cs = new CryptoStream(ms, rijn.CreateDecryptor(key, rgbIV),
            CryptoStreamMode.Write);

            cs.Write(encryptedTextBytes, 0, encryptedTextBytes.Length);

            cs.Close();

            return Encoding.UTF8.GetString(ms.ToArray());

        }

        public static string UpdateLogFile(string Message)
        {
            try
            {
                //use current directory
                string PathString = System.IO.Directory.GetCurrentDirectory();
                string FileName = "ConsoleApplicationErrorLog.txt";

                // Check if file already exists. If no, create it. 
                if (!System.IO.File.Exists(FileName))
                {
                    // Create a new file 
                    FileStream fs = System.IO.File.Create(PathString + @"\" + FileName);
                    fs.Close(); //close file

                    //Write message to log file
                    System.IO.File.AppendAllText(PathString + @"\" + FileName, DateTime.Now.ToString() + ": File Created." + Environment.NewLine);
                }

                //Write message to log file
                System.IO.File.AppendAllText(PathString + @"\" + FileName, DateTime.Now.ToString() + ": " + Message + Environment.NewLine);

                return ("true");
            }
            catch (Exception Ex)
            {
                return (Ex.ToString());
            }
        }
    }
}
