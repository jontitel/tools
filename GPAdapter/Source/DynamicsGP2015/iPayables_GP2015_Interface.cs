﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.IO;
using System.Linq;
using DM_Helpers;
using iPayables_Integration;
using Microsoft.Win32;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Configuration;

namespace ImportApplication
{
    class Program
    {
        static private DataTable dte;
        static private string conString = "";
        static private string DynamicsConString = "";

        public enum RunType
        {
            All = 0,
            InboundOnly = 1,
            OutboundOnly = 2
        }

        public struct CommandArgs
        {
            public string CompanyID;
            public int RunOption;
            public string ImportMask;
        }

        public struct CompanySettings
        {
            public int GPCompanyID;
            public string DatabaseName;
            public string CheckBook;
            public int PostingDateOption;
            public string DefaultAPAccount;
            public int UsesPurchaseOrders;
        }


        [STAThread]
        static void Main(string[] args)
        {
            NameValueCollection appSettings = ConfigurationManager.AppSettings;
            //RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\iPayablesService");
            Sql sql = new Sql();
            iPayables_Helpers iPayHelp = new iPayables_Helpers();
            string[] files;
            DataTable FtpInfo = new DataTable();
            bool validArgs=true;
            CommandArgs runArgs = LoadCommandArgs(args,out validArgs);

            if (validArgs == false)
            {
                return;
            }

            string[] lines = { "" };
            string Database = "";
            string companyName = "";
            string customerID = appSettings["CustomerID"];
            string logPath = appSettings["LogPath"];
            string logFile = logPath + @"\GP-IWInterfaceLog_" + DateTime.Now.ToString("yyyyMMddhhnnss") + ".log";
            CompanySettings companySettings = new CompanySettings();

            DM_Helpers.InstallHelper.UpdateLogFile(logFile, "Starting Import Application...");
            
            try
            {
                // Get Dynamics Connection Info
                string SqlServer = appSettings["SQLServer"];
                DynamicsConString = "Data Source=" + SqlServer + ";Initial Catalog=" + "DYNAMICS" + ";integrated security=SSPI;persist security info=False;packet size=4096";
                
                // Lookup the Company Database info. Will use company if provided in Run Time parameters
                companySettings = LookupCompanySettings(runArgs.CompanyID,DynamicsConString);
                DM_Helpers.InstallHelper.UpdateLogFile(logFile, "Sql Connection to Dynamics Database established");

                Database = companySettings.DatabaseName;
                DM_Helpers.InstallHelper.UpdateLogFile(logFile, "Company Database " + Database + " found in companySettings.");
                conString = "Data Source=" + SqlServer + ";Initial Catalog=" + Database + ";integrated security=SSPI;persist security info=False;packet size=4096";
                
                // Get Company Name from GP
                companyName = LookupCompanyName(companySettings.GPCompanyID, conString);
                DM_Helpers.InstallHelper.UpdateLogFile(logFile, "Sql Connection to Company Database established.");
                
            }
            catch (Exception Ex)
            {
                DM_Helpers.InstallHelper.UpdateLogFile(logFile, "Error connecting Sql Data: " + Ex.ToString());
                return;
            }


            //Pull data from stored procedures, dump into xml, upload xml to server
            if((RunType)runArgs.RunOption == RunType.All || (RunType)runArgs.RunOption == RunType.OutboundOnly)
            {
                    try
                    {
                        string timeStamp = DateTime.Now.ToString("yyyyMMddhhmmss");

                        //Vendors
                        dte = sql.QueryDatabase(DynamicsConString, "exec usp_iPayables_GetVendors '" + Database + "'");
                        iPayHelp.ExportVendors(dte, customerID, runArgs.CompanyID, companyName, appSettings, timeStamp);

                        //Purchase Orders
                        if (companySettings.UsesPurchaseOrders == 1)
                        {
                            dte = sql.QueryDatabase(DynamicsConString, "exec usp_iPayables_PurchaseOrder_Lookup '" + Database + "'");
                            iPayHelp.ExportPurchaseOrders(dte, customerID, runArgs.CompanyID, companyName, appSettings, timeStamp);
                        }

                        //Status
                        dte = sql.QueryDatabase(DynamicsConString, "exec usp_iPayables_GetInvoices '" + Database + "'");
                        iPayHelp.ExportGLInvoices(dte, customerID, runArgs.CompanyID, appSettings, timeStamp);

                        //Codes
                        dte = sql.QueryDatabase(DynamicsConString, "exec usp_iPayables_GetGLAccounts '" + Database + "'");
                        iPayHelp.ExportGLAccounts(dte, customerID, runArgs.CompanyID, appSettings, timeStamp);

                        DM_Helpers.InstallHelper.UpdateLogFile(logFile, "Pulled Data From Stored Procedures...");
                    }
                    catch (Exception Ex)
                    {
                        DM_Helpers.InstallHelper.UpdateLogFile(logFile, "Error pulling data from stored procedures: " + Ex.ToString());
                        return;
                    }
                    
            }
            //Import invoices into GP from iPayables
            if ((RunType)runArgs.RunOption == RunType.All || (RunType)runArgs.RunOption == RunType.InboundOnly)
            {
                try
                {
                    //get list of files
                    files = Directory.GetFiles(appSettings["ImportFolder"]); 

                    DM_Helpers.InstallHelper.UpdateLogFile(logFile, files.Count().ToString() + " invoice files found matching the ImportMast of " + runArgs.ImportMask);
                }
                catch (Exception Ex)
                {
                    DM_Helpers.InstallHelper.UpdateLogFile(logFile, "Error downloading invoices from FTP server: " + Ex.ToString());
                }

                //import invoice to GP and send status log to server
                try
                {
                    files = Directory.GetFiles(appSettings["ImportFolder"], runArgs.ImportMask);
                       
                    //import to gp
                    foreach (string file in files)
                    {
                        DM_Helpers.InstallHelper.UpdateLogFile(logFile, "File: " + file);
                        iPayHelp.ImportInvoice(file, conString, DynamicsConString,runArgs.CompanyID,companySettings,logPath,appSettings);
                        //try{
                        //
                            //send status log to server
                            //File.Move(appSettings["WorkingFolder"] + Path.GetFileNameWithoutExtension(file) + "_Results.xml",appSettings["WorkingFolder"] + "/Outbound/");
                            //DM_Helpers.InstallHelper.UpdateLogFile(logFile, "Status Log Sent To Server...");
                        //}
                        //catch (Exception ex)
                        //{
                        //    DM_Helpers.InstallHelper.UpdateLogFile(logFile, "Error sending status log to server: " + ex.ToString());
                        //    return;
                        //}

                        //delete files when finished
                        try
                        {
                            // Archive File
                            File.Copy(file, appSettings["ArchiveFolder"] + Path.GetFileName(file));

                            DM_Helpers.InstallHelper.DeleteFile(file);
                            DM_Helpers.InstallHelper.DeleteFile(logPath + @"\Export\LoadStatus.xml");
                            DM_Helpers.InstallHelper.UpdateLogFile(logFile, "Import File Deleted...");
                        }
                        catch (Exception ex)
                        {
                            DM_Helpers.InstallHelper.UpdateLogFile(logFile, "Error deleting file: " + ex.ToString());
                            return;
                        }
                    }

                    DM_Helpers.InstallHelper.UpdateLogFile(logFile, "Invoices Imported To GP!");
                }
                catch (Exception Ex)
                {
                    DM_Helpers.InstallHelper.UpdateLogFile(logFile, "Error importing invoices GP: " + Ex.ToString());
                        return;
                }
            }
            DM_Helpers.InstallHelper.UpdateLogFile(logFile, "End Of Import Application");
            
        }

        static CommandArgs LoadCommandArgs(string[] Parameters, out bool ValidArguments)
        {
            CommandArgs runArgs = new CommandArgs();
            int idx = 0;
            bool validArgs = true;

            if (Parameters.Length == 0)
            {
            runArgs.RunOption = 0;
            runArgs.ImportMask = "";
                runArgs.CompanyID = "";
            }

            for (idx = 0; idx< Parameters.Length;idx++)
            {
                switch (Parameters[idx])
                {
                    case "/company":
                        runArgs.CompanyID = Parameters[idx + 1];
                        idx += 1;
                        break;
                    case "/runoption":
                        runArgs.RunOption = Convert.ToInt32(Parameters[idx + 1]);
                        idx += 1;
                        break;
                    case "/importmask":
                        runArgs.ImportMask = Parameters[idx + 1];
                        idx += 1;
                        break;
                    default:
                        validArgs = false;
                        break;
                }
            }

            if (validArgs == false)
            {
                Console.WriteLine("Invalid Command Line Parameter supplied.  Valid options are:");
                Console.WriteLine("/company CompanyCode  Company Code that Payables is run out of");
                Console.WriteLine("/runoption 0|1|2 0=All, 1=Inbound Only (Invoice Import), 2=Outbound Only (Vendor, Code and Status)");
                Console.WriteLine("/importmask ImportMask Specify the file mask for inbound files.  Default is '*.xml'");
            }

            // Check if Import Mask is provided for inbound processing. If not, default to "*.xml"
            if ((RunType)runArgs.RunOption != RunType.OutboundOnly)
            {
                if (runArgs.ImportMask == null || runArgs.ImportMask.Length == 0)
                {
                    runArgs.ImportMask = "*.xml";
                }
            }
            ValidArguments = validArgs;
            return runArgs;
        }
        static CompanySettings LookupCompanySettings(string CompanyID, string DynamicsConnectString)
        {
            CompanySettings settings = new CompanySettings();
            Sql sql = new Sql();

            DataTable info = sql.QueryDatabase(DynamicsConString, "EXEC usp_iPayables_CompanySettings_Lookup '" + CompanyID + "'");

            if (info.Rows.Count > 0)
            {
                settings.DatabaseName = info.Rows[0]["DatabaseName"].ToString();
                settings.CheckBook = info.Rows[0]["Checkbook"].ToString();
                settings.PostingDateOption = Convert.ToInt32(info.Rows[0]["OverridePostingDate"]);
                settings.GPCompanyID = Convert.ToInt32(info.Rows[0]["DynamicsCompanyID"]);
                settings.DefaultAPAccount = info.Rows[0]["DefaultAPAccount"].ToString();
                if (info.Columns.Contains("UsesPurchaseOrder") == true)
                {
                    settings.UsesPurchaseOrders = Convert.ToInt32(info.Rows[0]["UsesPurchaseOrder"]);
                }
                else
                {
                    settings.UsesPurchaseOrders = 0;
                }
            }

            return settings;
        }
        static string LookupCompanyName(int GPCompanyID, string DynamicsConnectString)
        {
            Sql sql = new Sql();

            DataTable info = sql.QueryDatabase(DynamicsConString, "EXEC usp_iPayables_GPCompany_Lookup " + GPCompanyID);

            if (info.Rows.Count > 0)
            {
                return info.Rows[0]["CMPNYNAM"].ToString().TrimEnd();
            }
            else
            {
                return "";
            }
        }
    }

}
