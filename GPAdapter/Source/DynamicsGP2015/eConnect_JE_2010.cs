﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Dynamics.GP.eConnect;
using Microsoft.Dynamics.GP.eConnect.Serialization;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Data.SqlClient;
//using Microsoft.Dynamics.GP.eConnect.MiscRoutines;

namespace ImportApplication
{
    class eConnect_JE
    {
        private taGLTransactionHeaderInsert header;
        private List<taGLTransactionLineInsert_ItemsTaGLTransactionLineInsert> lines;
        private List<taAnalyticsDistribution_ItemsTaAnalyticsDistribution> aaLines;
        private string connectionString;
        int lineSequence;
        public string Errors { get; set; }
        public int Success { get; set; }
        public int Failure { get; set; }

        public eConnect_JE(string conString)
        {
            Errors = "";
            connectionString = conString;
            lineSequence = 0;
            Success = 0;
            Failure = 0;
            lines = new List<taGLTransactionLineInsert_ItemsTaGLTransactionLineInsert>();
            aaLines = new List<taAnalyticsDistribution_ItemsTaAnalyticsDistribution>();
            header = new taGLTransactionHeaderInsert();
        }

        public void ResetVariables()
        {
            Errors = "";
            lineSequence = 0;
            Success = 0;
            Failure = 0;
            lines = new List<taGLTransactionLineInsert_ItemsTaGLTransactionLineInsert>();
            aaLines = new List<taAnalyticsDistribution_ItemsTaAnalyticsDistribution>();
            header = new taGLTransactionHeaderInsert();
        }

        public void SetTransactionHeader(string TXDate, string Batch, string Reference, short transactionType, string reversingDate, string[] UserDefinedFieldValues)
        {
            GetNextDocNumbers docNumbers = new GetNextDocNumbers();
            header.JRNENTRY = Convert.ToInt32(docNumbers.GetNextGLJournalEntryNumber(IncrementDecrement.Increment, connectionString));
            //header.JRNENTRY = Convert.ToInt32(docNumbers.GetNextGLJournalEntryNumber(Microsoft.Dynamics.GP.eConnect.MiscRoutines.GetNextDocNumbers.IncrementDecrement.Increment, connectionString)); ;
            header.BACHNUMB = Batch;
            header.TRXDATE = TXDate;
            header.REFRENCE = Reference;
            header.TRXTYPE = transactionType;
            header.RVRSNGDT = reversingDate;

			// Insert any values configured for User Defined Fields
			if (UserDefinedFieldValues[1].Length > 0) header.USRDEFND1 = UserDefinedFieldValues[1];
			if (UserDefinedFieldValues[2].Length > 0) header.USRDEFND2 = UserDefinedFieldValues[2];
			if (UserDefinedFieldValues[3].Length > 0) header.USRDEFND3 = UserDefinedFieldValues[3];
			if (UserDefinedFieldValues[4].Length > 0) header.USRDEFND4 = UserDefinedFieldValues[4];
			if (UserDefinedFieldValues[5].Length > 0) header.USRDEFND5 = UserDefinedFieldValues[5];

        }

        public void InsertDistribution(string AccountNumber, decimal Debit, decimal Credit, string Description, string OriginalMstrNum, string OriginalMstrName)
        {
            lineSequence += 16384;
            taGLTransactionLineInsert_ItemsTaGLTransactionLineInsert line = new taGLTransactionLineInsert_ItemsTaGLTransactionLineInsert();

            line.BACHNUMB = header.BACHNUMB;
            line.JRNENTRY = header.JRNENTRY;
            line.DEBITAMT = Debit;
            line.CRDTAMNT = Credit;
            line.ACTNUMST = AccountNumber.Trim();
            line.SQNCLINE = lineSequence;
            line.DSCRIPTN = Description;
            line.ORDOCNUM = OriginalMstrNum;
            line.ORMSTRNM = OriginalMstrName;

            lines.Add(line);
        }

        public void InsertDistribution(string AccountNumber, decimal Debit, decimal Credit, string Description,
            string OriginatingMasterName, string OriginatingTXDescription, string OriginatingMasterID)
        {
            lineSequence += 16384;
            taGLTransactionLineInsert_ItemsTaGLTransactionLineInsert line = new taGLTransactionLineInsert_ItemsTaGLTransactionLineInsert();

            line.BACHNUMB = header.BACHNUMB;
            line.JRNENTRY = header.JRNENTRY;
            line.DEBITAMT = Debit;
            line.CRDTAMNT = Credit;
            line.ACTNUMST = AccountNumber.Trim();
            line.SQNCLINE = lineSequence;
            line.DSCRIPTN = Description;
            line.ORMSTRID = OriginatingMasterID;
            line.ORMSTRNM = OriginatingMasterName;
            line.ORTRXDESC = OriginatingTXDescription;
            lines.Add(line);
        }


        public void InsertAADistribution(string Account, decimal amount, string Reference, string Dimension, string Code)
        {
            taAnalyticsDistribution_ItemsTaAnalyticsDistribution aaLine = new taAnalyticsDistribution_ItemsTaAnalyticsDistribution();
            aaLine.ACTNUMST = Account.Trim();
            aaLine.AMOUNTSpecified = true;
            aaLine.AMOUNT = amount;
            aaLine.DistRef = Reference;
            aaLine.DistSequenceSpecified = true;
            aaLine.DistSequence = lineSequence;
            aaLine.aaTrxDim = Dimension;
            aaLine.DOCNMBR = header.JRNENTRY.ToString();
            aaLine.DOCTYPE = 0;
            aaLine.aaTrxDimCode = Code;

            aaLines.Add(aaLine);
        }


        public void InsertTransaction()
        {
            eConnectType eConnect = new eConnectType();
            GLTransactionType[] glArray = new GLTransactionType[1];
            GLTransactionType glType = new GLTransactionType();

            glType.taGLTransactionHeaderInsert = header;
            glType.taGLTransactionLineInsert_Items = lines.ToArray();
            glType.taAnalyticsDistribution_Items = aaLines.ToArray();
            glArray[0] = glType;
            eConnect.GLTransactionType = glArray;

            if (XMLSerialize("JE.xml", eConnect))
            {
                if (eConnectEntry("JE.xml"))
                {
                    Success++;
                }
                else
                {
                    Failure++;
                }
            }
        }

        private bool eConnectEntry(string filename)
        {

            //return true;

            bool message = false;

            eConnectMethods eConnect = new eConnectMethods();
            XmlDocument myXmlDoc = new XmlDocument();
            XmlNode eConnectProcessInfoOutgoing;
            try
            {
                myXmlDoc.Load(filename);
                eConnectProcessInfoOutgoing = myXmlDoc.SelectSingleNode("//Outgoing");
                //use top if GP 2010, use bottom if GP 10
                eConnect.CreateTransactionEntity(connectionString, myXmlDoc.OuterXml);
                //eConnect.eConnect_EntryPoint(connectionString, EnumTypes.ConnectionStringType.SqlClient, myXmlDoc.OuterXml, EnumTypes.SchemaValidationType.None, "");
                message = true;
            }
            catch (Exception ex)
            {
                message = false;
                Errors += ex.Message.ToString() + "\n";
            }
            return message;

        }

        private bool XMLSerialize(string filename, eConnectType eConnect)
        {
            try
            {
                // Create a file to hold the serialized eConnect XML document
                FileStream fs = new FileStream(filename, FileMode.Create);
                XmlTextWriter writer = new XmlTextWriter(fs, new UTF8Encoding());

                // Serialize the eConnect document object to the file using the XmlTextWriter.
                XmlSerializer serializer = new XmlSerializer(eConnect.GetType());
                serializer.Serialize(writer, eConnect);
                writer.Close();
                return true;
            }
            catch (Exception ex)
            {
                Errors += ex.Message.ToString() + Environment.NewLine;
                return false;
            }
        }
    }
}

